DROP TABLE IF EXISTS lot_image CASCADE;
DROP TABLE IF EXISTS bid CASCADE;
DROP TABLE IF EXISTS lot CASCADE;
DROP TABLE IF EXISTS users CASCADE;


--User
CREATE TABLE users
(
    id         SERIAL PRIMARY KEY,
    created_at TIMESTAMP   NOT NULL DEFAULT current_timestamp,
    updated_at TIMESTAMP   NOT NULL DEFAULT current_timestamp,


    email      VARCHAR(50) NOT NULL UNIQUE,
    password   VARCHAR(50) NOT NULL,
    role       VARCHAR(20) NOT NULL DEFAULT 'USER',
    status     VARCHAR(20) NOT NULL DEFAULT 'ACTIVE'
);

--Super Admin
INSERT INTO users (email, password, role, status)
VALUES ('jbekzod2004@gmail.com',
        '106k103h103h108m119x120y121z', 'SUPER_ADMIN', 'ACTIVE');


--Lot
CREATE TABLE lot
(
    id           SERIAL PRIMARY KEY,
    created_at   TIMESTAMP        NOT NULL DEFAULT current_timestamp,
    updated_at   TIMESTAMP        NOT NULL DEFAULT current_timestamp,


    name         VARCHAR(100)     NOT NULL,
    description  VARCHAR(500)     NOT NULL,
    owner_id     INT              NOT NULL REFERENCES users (id) ON DELETE CASCADE ,
    status       VARCHAR(20)      NOT NULL DEFAULT 'NEW',
    auction_type VARCHAR(20)      NOT NULL DEFAULT 'DIRECT',
    started_at   TIMESTAMP        NOT NULL CHECK ( started_at > lot.created_at ),
    finish_at    TIMESTAMP        NOT NULL,
    CHECK ( finish_at > lot.started_at ),
    UNIQUE (name, description, started_at,finish_at),
    start_price  DOUBLE PRECISION NOT NULL CHECK ( start_price >= 0 )
);

--ItemImage
CREATE TABLE lot_image
(

    id         SERIAL PRIMARY KEY,
    created_at TIMESTAMP   NOT NULL DEFAULT current_timestamp,
    updated_at TIMESTAMP   NOT NULL DEFAULT current_timestamp,


    lot_id     INT         NOT NULL REFERENCES lot (id) ON DELETE CASCADE ,
    source     VARCHAR(60) NOT NULL UNIQUE
);

--Bid
CREATE TABLE bid
(
    id         SERIAL PRIMARY KEY,
    created_at TIMESTAMP        NOT NULL DEFAULT current_timestamp,
    updated_at TIMESTAMP        NOT NULL DEFAULT current_timestamp,


    owner_id   INT              NOT NULL REFERENCES users (id) ON DELETE CASCADE ,
    lot_id     INT              NOT NULL REFERENCES lot (id) ON DELETE CASCADE ,
    amount     DOUBLE PRECISION NOT NULL CHECK ( amount >= 0 ),

    UNIQUE ( lot_id,amount )
);


-- -- Sample Users
-- INSERT INTO users (email, password)
-- VALUES ('user@gmail.com',
--         '106k103h103h108m119x120y121z');
-- INSERT INTO users (email, password)
-- VALUES ('user1@gmail.com',
--         '106k103h103h108m119x120y121z');
-- INSERT INTO users (email, password)
-- VALUES ('user2@gmail.com',
--         '106k103h103h108m119x120y121z');
-- INSERT INTO users (email, password)
-- VALUES ('admin@gmail.com',
--         '106k103h103h108m119x120y121z');
--
--
-- --Sample Lots
-- INSERT INTO lot(name, description, owner_id, started_at, auction_type, finish_at, start_price)
-- VALUES ('Chevrolet Cobalt', 'created at 2017 and not crashed', 2, '2023-01-01 00:00:00.001', 'DIRECT',
--         '2023-02-01 00:00:00.001', 9000);
--
-- INSERT INTO lot(name, description, owner_id, started_at, auction_type, finish_at, start_price)
-- VALUES ('Bike', 'Surkhan Velo ', 2, '2023-12-01 12:00:00.001', 'DIRECT', '2024-03-01 00:00:00.001', 900);
--
-- INSERT INTO lot(name, description, owner_id, started_at, auction_type, finish_at, start_price)
-- VALUES ('Portrait of Monaliza', 'Copy version of Actual one', 2, '2023-02-12 00:37:00.001', 'DIRECT',
--         '2023-02-20 00:00:00.001', 3000);
--
-- INSERT INTO lot(name, description, owner_id, started_at, auction_type, finish_at, start_price)
-- VALUES ('House with 10 rooms', 'Built in 2000 and it is located near the river', 2, '2023-01-01 01:06:00.001', 'DIRECT',
--         '2023-01-20 01:06:00.001', 50000);
--
-- INSERT INTO lot(name, description, owner_id, started_at, auction_type, finish_at, start_price)
-- VALUES ('Hallucination By Graff Diamonds',
--         'The watch was first displayed in 2014 and if you are wanting the world''s most expensive, elegant and glamorous timepiece in your possession it will set you back a hair raising $55 million!',
--         2, '2022-12-12 00:37:00.001', 'DIRECT', '2023-01-02 00:37:00.001', 550000000);
--
-- INSERT INTO lot(name, description, owner_id, started_at, auction_type, finish_at, start_price)
-- VALUES ('Ferrari', 'Actual version of 1900', 2, '2023-02-12 00:37:00.001', 'DIRECT', '2023-02-15 00:00:00.001', 100000);
--
-- INSERT INTO lot(name, description, owner_id, started_at, auction_type, finish_at, start_price)
-- VALUES ('Impress Zeon 4G',
--         'mobile device by Vertex announced in 2018. : 72 x 150 x 8.7 mm · : 190 g ; Impress Luck NFC · : 73 x 144 x 9.5 mm · : 145 g ; Impress Aqua.',
--         2, '2023-01-16 08:41:00.001', 'DIRECT', '2023-02-12 00:37:00.001', 9000);
--
-- INSERT INTO lot(name, description, owner_id, started_at, auction_type, finish_at, start_price)
-- VALUES ('Ball', 'This ball is one of the balls that Messi scored by', 2, '2023-01-7 11:25:38.001', 'DIRECT',
--         '2023-02-09 12:01:07.001', 9000);