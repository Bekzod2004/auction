package uz.pdp.auction.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ImagesRootPath {
    private static final String IMAGES_ROOT_PATH;
    private static final String imagesKey = "images.url";
    private ImagesRootPath() {
    }

    public static String imagesRootPath(){
        return IMAGES_ROOT_PATH;
    }
    static {
        try (InputStream propertiesStream = ImagesRootPath.class.getClassLoader().getResourceAsStream("application.properties")) {
            Properties properties = new Properties();
            properties.load(propertiesStream);
            IMAGES_ROOT_PATH = properties.getProperty(imagesKey);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
