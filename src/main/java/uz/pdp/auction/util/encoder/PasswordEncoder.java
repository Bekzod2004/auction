package uz.pdp.auction.util.encoder;

public final class PasswordEncoder {

    public static String encode(String password) {
        int len = password.length();
        StringBuilder builder = new StringBuilder(2 * len);
        for (int i = 0; i < len; i++)
            builder.append((char)( (password.charAt(i) + 25))%26 +'a').append((char) (password.charAt(i) % 26 + 'a'));
        return builder.toString();
    }
}
