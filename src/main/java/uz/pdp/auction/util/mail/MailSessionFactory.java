package uz.pdp.auction.util.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class MailSessionFactory {
    private final Properties properties;
    private static MailSessionFactory factory;

    private static final String NAME_PROPERTY = "mail.smtp.user";
    private static final String PASSWORD_PROPERTY = "mail.smtp.password";

    private final String name;
    private final String password;

    public static MailSessionFactory getInstance(){
        if (factory == null) {
            Properties p = new Properties();
            try (InputStream mailSessionPropertiesInputStream = MailSessionFactory.class.getClassLoader().getResourceAsStream("mail.properties")) {
                p.load(mailSessionPropertiesInputStream);
                factory = new MailSessionFactory(p);
            } catch (IOException e) {
                throw new RuntimeException("mail.properties  file could not be read");
            }
        }
        return factory;
    }
    private MailSessionFactory(Properties properties) {
        this.properties = properties;
        name = properties.getProperty(NAME_PROPERTY);
        password = properties.getProperty(PASSWORD_PROPERTY);
    }

    Session createSession() {
        return Session.getDefaultInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(name, password);
            }
        });
    }
}
