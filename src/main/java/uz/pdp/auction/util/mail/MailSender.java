package uz.pdp.auction.util.mail;

import uz.pdp.auction.exception.CustomException;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailSender {

    private  final MailSessionFactory factory = MailSessionFactory.getInstance();
    private static MailSender instance;

    private MailSender() {
    }

    public static MailSender getInstance() {
        if (instance == null) {
            instance = new MailSender();
        }
        return instance;
    }

    public boolean sendMail(String recipient, String subject, String text) throws CustomException {
        try {
            Session session = factory.createSession();
            MimeMessage message = new MimeMessage(session);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
            message.setSubject(subject);
            message.setContent(text,"text/html; charset=UTF-8");
            Transport.send(message);
            System.out.printf("Message sent to %s", recipient);
            return true;
        } catch (AddressException e) {
            System.err.printf("Invalid recipient address: %s. Cause: %s", recipient, e.getMessage());
            throw new CustomException(e.getMessage());
        } catch (MessagingException e) {
            System.err.printf("Messaging exception: %s", e.getMessage());
            throw new CustomException(e.getMessage());
        }
    }

}
