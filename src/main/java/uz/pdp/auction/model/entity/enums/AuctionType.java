package uz.pdp.auction.model.entity.enums;

public enum AuctionType {
    DIRECT,
    REVERSE
}
