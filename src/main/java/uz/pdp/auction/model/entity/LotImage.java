package uz.pdp.auction.model.entity;

import lombok.Getter;
import lombok.Setter;
import uz.pdp.auction.model.entity.abs.AbsEntity;

@Getter
@Setter
public class LotImage extends AbsEntity {
    private Lot lot;
    private String source;
}
