package uz.pdp.auction.model.entity;

import lombok.*;
import uz.pdp.auction.model.entity.abs.AbsEntity;
import uz.pdp.auction.model.entity.enums.AuctionType;
import uz.pdp.auction.model.entity.enums.LotStatus;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Bid extends AbsEntity {
    private User owner;
    private Lot lot;
    private double amount;
}
