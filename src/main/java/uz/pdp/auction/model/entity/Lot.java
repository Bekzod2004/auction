package uz.pdp.auction.model.entity;

import lombok.*;
import uz.pdp.auction.model.entity.abs.AbsEntity;
import uz.pdp.auction.model.entity.enums.AuctionType;
import uz.pdp.auction.model.entity.enums.LotStatus;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class Lot extends AbsEntity {
    private String name;
    private String description;
    private User owner;
    private LotStatus status;
    private AuctionType auctionType;
    private double startPrice;
    private LocalDateTime startedAt;
    private LocalDateTime finishAt;

    public Lot() {
    }

    public Lot(String name, String description, User owner, double startPrice, AuctionType auctionType, LocalDateTime startedAt, LocalDateTime finishAt) {
        this.name = name;
        this.description = description;
        this.owner = owner;
        this.auctionType = auctionType;
        this.startPrice = startPrice;
        this.startedAt = startedAt;
        this.finishAt = finishAt;
    }

    public Lot(Integer id, String name, String description, User owner, double startPrice, AuctionType auctionType, LocalDateTime startedAt, LocalDateTime finishAt) {
        super(id);
        this.name = name;
        this.description = description;
        this.owner = owner;
        this.auctionType = auctionType;
        this.startPrice = startPrice;
        this.startedAt = startedAt;
        this.finishAt = finishAt;
    }
}
