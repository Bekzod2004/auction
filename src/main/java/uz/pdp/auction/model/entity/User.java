package uz.pdp.auction.model.entity;

import lombok.*;
import uz.pdp.auction.model.entity.abs.AbsEntity;
import uz.pdp.auction.model.entity.enums.UserRole;
import uz.pdp.auction.model.entity.enums.UserStatus;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User extends AbsEntity {
    private String email;
    private String password;
    private UserRole role;
    private UserStatus status;

    public User(String email,String password){
        this.email = email;
        this.password = password;
    }
}
