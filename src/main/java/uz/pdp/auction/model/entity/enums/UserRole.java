package uz.pdp.auction.model.entity.enums;

public enum UserRole {
    SUPER_ADMIN,
    ADMIN,
    USER
}
