package uz.pdp.auction.model.entity.enums;

public enum UserStatus {
    ACTIVE,
    BLOCKED
}
