package uz.pdp.auction.model.entity.enums;

public enum LotStatus {
    NEW,
    ACTIVE,
    SUSPENDED,
    SOLD,
}
