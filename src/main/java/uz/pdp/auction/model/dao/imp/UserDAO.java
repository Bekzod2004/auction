package uz.pdp.auction.model.dao.imp;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.connection.MyConnectionPool;
import uz.pdp.auction.model.dao.Dao;
import uz.pdp.auction.model.dao.UserDao;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.UserRole;
import uz.pdp.auction.model.entity.enums.UserStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDAO implements UserDao {

    private static final String INSERT = "INSERT INTO users(email, password)VALUES(?,?);";
    private static final String FIND_BY_ID = "SELECT id, email, role, status FROM users WHERE id = ?;";
    private static final String FIND_BY_EMAIL = "SELECT id, email, role, status FROM users WHERE email = ?;";
    private static final String FIND_ALL = "SELECT id, email, role, status FROM users";
    private static final String CHANGE_STATUS = "UPDATE users SET status = ?, updated_at = current_timestamp WHERE id = ?;";
    private static final String CHANGE_ROLE = "UPDATE users SET role = ?, updated_at = current_timestamp WHERE id = ?;";
    private static final String CHANGE_PASSWORD = "UPDATE users SET password = ?, updated_at = current_timestamp WHERE id = ?;";
    private static final String CHANGE_PASSWORD_BY_EMAIL = "UPDATE users SET password = ?, updated_at = current_timestamp WHERE email = ?;";
    private static final String AUTHENTICATE = "SELECT id, email, role, status FROM users WHERE email = ? AND password = ?;";
    private static final String DELETE = "DELETE  FROM users WHERE id = ?;";
    private static UserDAO userDao;

    private UserDAO() {
    }

    public static UserDAO getInstance() {
        if (userDao == null)
            userDao = new UserDAO();
        return userDao;
    }

    @Override
    public boolean insert(User user) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(INSERT)) {
            return executeUpdatePrepareStatement(ps, Dao.STRING + user.getEmail(), Dao.STRING + user.getPassword());
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public boolean delete(int id) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(DELETE)) {
            return putArgs(ps, Dao.INTEGER + id).executeUpdate() > 0;
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public Optional<User> findById(int id) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            return getOptionalUser(executePrepareStatement(ps, Dao.INTEGER + id));
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public boolean update(User user) throws CustomException {
        throw new CustomException("user can not be fully updated");
    }

    @Override
    public boolean updatePassword(int id, String password) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(CHANGE_PASSWORD)) {
            return executeUpdatePrepareStatement(ps, Dao.STRING + password, Dao.INTEGER + id);
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public boolean changePasswordByEmail(String email, String password) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(CHANGE_PASSWORD_BY_EMAIL)) {
            return executeUpdatePrepareStatement(ps, Dao.STRING + password, Dao.STRING + email);
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public boolean changeRole(int id, UserRole role) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(CHANGE_ROLE)) {
            return executeUpdatePrepareStatement(ps, Dao.STRING + role.name(), Dao.INTEGER + id);
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public boolean changeStatus(int id, UserStatus status) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(CHANGE_STATUS)) {
            return executeUpdatePrepareStatement(ps, Dao.STRING + status.name(), Dao.INTEGER + id);
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public Optional<User> findByEmail(String email) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(FIND_BY_EMAIL)) {
            return getOptionalUser(executePrepareStatement(ps, Dao.STRING + email));
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public Optional<User> authenticate(String email, String password) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(AUTHENTICATE)) {
            return getOptionalUser(executePrepareStatement(ps, Dao.STRING + email, Dao.STRING + password));
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public List<User> findAll() throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             Statement s = connection.createStatement()) {
            return getPagesOfUsers(s.executeQuery(FIND_ALL));
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    private ResultSet executePrepareStatement(PreparedStatement ps, String... args) throws SQLException {
        return putArgs(ps, args).executeQuery();
    }

    private boolean executeUpdatePrepareStatement(PreparedStatement ps, String... args) throws SQLException {
        return putArgs(ps, args).executeUpdate() > 0;
    }

    private PreparedStatement putArgs(PreparedStatement ps, String... args) throws SQLException {
        return Dao.setPSArgs(ps, args);
    }

    private Optional<User> getOptionalUser(ResultSet rs) throws CustomException {
        try {
            if (rs.next()) {
                User user;
                user = getUserFromResultSet(rs);
                if (user.getId() != null)
                    return Optional.of(user);
            }
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
        return Optional.empty();
    }

    private User getUserFromResultSet(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(Integer.valueOf(rs.getString("id")));
        user.setEmail(rs.getString("email"));
        user.setRole(UserRole.valueOf(rs.getString("role")));
        user.setStatus(UserStatus.valueOf(rs.getString("status")));
        return user;
    }

    private List<User> getPagesOfUsers(ResultSet rs) throws CustomException {
        List<User> users = new ArrayList<>();
        while (true)
            try {
                if (!rs.next()) break;
                users.add(getUserFromResultSet(rs));
            } catch (SQLException e) {
                throw new CustomException(e.getMessage());
            }
        return users;
    }
}
