package uz.pdp.auction.model.dao.imp;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.connection.MyConnection;
import uz.pdp.auction.model.connection.MyConnectionPool;
import uz.pdp.auction.model.dao.BidDao;
import uz.pdp.auction.model.dao.Dao;
import uz.pdp.auction.model.entity.Bid;
import uz.pdp.auction.model.entity.Lot;
import uz.pdp.auction.model.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BidDAO implements BidDao {
    private static BidDAO bidDAO;
    private static final String INSERT = "INSERT INTO bid(owner_id, lot_id, amount)VALUES(?, ?, ?);";
    private static final String FIND_BY_ID = "SELECT id, owner_id, lot_id, amount FROM bid WHERE id = ?;";
    private static final String FIND_BY_OWNER_ID = "SELECT b.id, b.owner_id, b.lot_id, b.amount, l.name, b.updated_at FROM bid b JOIN lot l on l.id = b.lot_id WHERE b.owner_id = ?;";
    private static final String FIND_BY_LOT_ID = "SELECT id, owner_id, lot_id, amount FROM bid WHERE lot_id = ?;";

    private static final String FIND_COUNT_BY_LOT_ID = "SELECT count(id) FROM bid WHERE lot_id = ?;";

    private static final String FIND_WINNER_BY_LOT_ID = "SELECT id, owner_id, lot_id, amount FROM bid WHERE amount = (SELECT CASE  WHEN l.auction_type = 'DIRECT' THEN max(bid.amount) ELSE min(bid.amount) END FROM bid  JOIN lot l on l.id = bid.lot_id WHERE lot_id = ? GROUP BY auction_type)";


    private BidDAO() {
    }

    public static BidDAO getInstance() {
        if (bidDAO == null)
            bidDAO = new BidDAO();
        return bidDAO;
    }

    @Override
    public List<Bid> findByLotId(Integer lotId) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(FIND_BY_LOT_ID)) {
            return getBidsFromResultSet(Dao.setPSArgs(ps, Dao.INTEGER + lotId).executeQuery());
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public Integer findSizeByLotId(Integer lotId) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(FIND_COUNT_BY_LOT_ID)) {
            ResultSet rs = Dao.setPSArgs(ps, Dao.INTEGER + lotId).executeQuery();
            int count = 0;
            if (rs.next())
                count = rs.getInt("count");
            return count;
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public List<Bid> findByOwnerId(Integer ownerId) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(FIND_BY_OWNER_ID)) {
            List<Bid> bids = new ArrayList<>();
            ResultSet rs = Dao.setPSArgs(ps, Dao.INTEGER + ownerId).executeQuery();
            while (rs.next()) {
                Bid bid = new Bid();
                bid.setId(rs.getInt("id"));
                User owner = new User();
                owner.setId(rs.getInt("owner_id"));
                bid.setOwner(owner);
                Lot lot = new Lot();
                lot.setId(rs.getInt("lot_id"));
                lot.setName(rs.getString("name"));
                bid.setLot(lot);
                bid.setAmount(rs.getDouble("amount"));
                bid.setUpdatedAt(rs.getTimestamp("updated_at").toLocalDateTime());
                bids.add(bid);
            }
            return bids;
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public Optional<Bid> findWinnerByLotId(Integer lotId) throws CustomException {
        try (MyConnection myConnection = (MyConnection) MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = myConnection.prepareStatement(FIND_WINNER_BY_LOT_ID)) {
            ResultSet rs = Dao.setPSArgs(ps, Dao.INTEGER + lotId).executeQuery();
            Bid bid;
            if (rs.next() && (bid = mapRsToBid(rs)).getId() != null)
                return Optional.of(bid);
            return Optional.empty();
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public boolean insert(Bid bid) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(INSERT)) {
            return Dao.setPSArgs(
                            ps,
                            Dao.INTEGER + bid.getOwner().getId(),
                            Dao.INTEGER + bid.getLot().getId(),
                            Dao.DOUBLE + bid.getAmount())
                    .executeUpdate() > 0;
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    /**
     * NOT Deletable.
     *
     * @param id ignored.
     * @return false.
     */
    @Override
    public boolean delete(int id) {
        return false;
    }

    @Override
    public Optional<Bid> findById(int id) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ResultSet rs = Dao.setPSArgs(ps, Dao.INTEGER + id).executeQuery();
            Bid bid;
            if (rs.next() && (bid = mapRsToBid(rs)).getId() != null)
                return Optional.of(bid);
            return Optional.empty();
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    /**
     * NOT Updatable.
     *
     * @param bid ignored.
     * @return false.
     */
    @Override
    public boolean update(Bid bid) {
        return false;
    }

    private List<Bid> getBidsFromResultSet(ResultSet rs) throws SQLException {
        List<Bid> bids = new ArrayList<>();
        while (rs.next())
            bids.add(mapRsToBid(rs));
        return bids;
    }

    private Bid mapRsToBid(ResultSet rs) throws SQLException {
        Bid bid = new Bid();
        bid.setId(rs.getInt("id"));
        User user = new User();
        user.setId(rs.getInt("owner_id"));
        bid.setOwner(user);
        Lot lot = new Lot();
        lot.setId(rs.getInt("lot_id"));
        bid.setLot(lot);
        bid.setAmount(rs.getDouble("amount"));
        return bid;
    }
}
