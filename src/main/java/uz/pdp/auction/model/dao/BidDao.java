package uz.pdp.auction.model.dao;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.Bid;

import java.util.List;
import java.util.Optional;

public interface BidDao extends Dao<Bid>{

    List<Bid> findByLotId(Integer lotId) throws CustomException;
    Integer findSizeByLotId(Integer lotId) throws CustomException;

    List<Bid> findByOwnerId(Integer ownerId) throws CustomException;

    Optional<Bid> findWinnerByLotId(Integer id) throws CustomException;
}
