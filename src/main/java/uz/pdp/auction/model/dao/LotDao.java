package uz.pdp.auction.model.dao;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.Lot;
import uz.pdp.auction.model.entity.enums.AuctionType;
import uz.pdp.auction.model.entity.enums.LotStatus;

import java.util.List;
import java.util.Optional;

public interface LotDao extends Dao<Lot> {

    /**
     * Find Lots by their owners' ids.
     *
     * @param ownerId Lot's ownerId.
     * @return List of lots if exists with this ownerId, otherwise empty List.
     * @throws CustomException if an error occurs.
     */
    List<Lot> findByOwnerId(Integer ownerId) throws CustomException;

    List<Lot> findAll()throws CustomException;

    List<Lot> getAllByAuctionTypeAndStatus(AuctionType auctionType, LotStatus status) throws CustomException;
    List<Lot> getAllByAuctionType(AuctionType auctionType) throws CustomException;
    List<Lot> getAllByStatus(LotStatus status) throws CustomException;

    Optional<Lot> findByNameDescriptionStartedAtFinishAt(Lot lot) throws CustomException;

    /**
     * Updates LotStatus of lot with given id.
     *
     * @param id     Lot id.
     * @param status LotStatus obj in (NEW, PROCESSING,  ACTIVE, SOLD,  WENT_OFF, BLOCKED).
     * @return true if there is a lot with this id and updated, otherwise false.
     * @throws CustomException if an error occurs.
     */
    boolean changeStatus(Integer id, LotStatus status) throws CustomException;
}
