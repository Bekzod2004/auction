package uz.pdp.auction.model.dao;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.LotImage;

import java.util.List;

public interface LotImageDao extends Dao<LotImage> {
    /**
     * Find All Images of a Lot.
     *
     * @param lotId Lot id.
     * @return List of ItemImage objs if images exist with this item_id. otherwise null.
     * @throws CustomException if an error occurs.
     */
    List<LotImage> findAllByLotId(Integer lotId) throws CustomException;

    boolean deleteByLotId(Integer lotId) throws CustomException;
}
