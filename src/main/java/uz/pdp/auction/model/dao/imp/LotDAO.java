package uz.pdp.auction.model.dao.imp;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.connection.MyConnectionPool;
import uz.pdp.auction.model.dao.Dao;
import uz.pdp.auction.model.dao.LotDao;
import uz.pdp.auction.model.entity.Lot;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.AuctionType;
import uz.pdp.auction.model.entity.enums.LotStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LotDAO implements LotDao {
    private static final String INSERT = "INSERT INTO lot(name, description , owner_id, start_price,auction_type, started_at, finish_at)VALUES(?,?,?,?,?,?,?);";
    private static final String FIND_BY_ID = "SELECT id, name, description, owner_id, status, start_price,auction_type, started_at, finish_at FROM lot WHERE id = ?;";

    private static final String FIND_ALL = "SELECT id, name, description, owner_id, status, start_price,auction_type, started_at, finish_at FROM lot;";

    private static final String FIND_BY_NAME_DESCRIPTION_STARTED_AT_FINISH_AT = "SELECT id, name, description, owner_id, status, start_price,auction_type, started_at, finish_at FROM lot WHERE name = ? AND description = ? AND started_at = ? AND finish_at = ?;";
    private static final String FIND_BY_OWNER_ID = "SELECT id, name, description, owner_id, status, start_price,auction_type, started_at, finish_at FROM lot WHERE owner_id = ?;";
    private static final String GET_ALL_BY_AUCTION_TYPE_AND_STATUS = "SELECT id, name, description, owner_id, status, start_price,auction_type, started_at, finish_at FROM lot WHERE status = ? AND auction_type = ?;";

    private static final String GET_ALL_BY_AUCTION_TYPE = "SELECT id, name, description, owner_id, status, start_price,auction_type, started_at, finish_at FROM lot WHERE auction_type = ?;";

    private static final String GET_ALL_BY_STATUS = "SELECT id, name, description, owner_id, status, start_price,auction_type, started_at, finish_at FROM lot WHERE status = ?;";
    private static final String CHANGE_STATUS = "UPDATE lot SET status = ?, updated_at = current_timestamp WHERE id = ?;";
    private static final String UPDATE = "UPDATE lot SET name = ?, description = ?, start_price = ?, started_at = ?, finish_at = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM lot WHERE id = ?;";
    private static LotDAO lotDao;

    private LotDAO() {
    }

    public static LotDAO getInstance() {
        if (lotDao == null)
            lotDao = new LotDAO();
        return lotDao;
    }

    @Override
    public boolean insert(Lot lot) throws CustomException {
        String[] args = {
                Dao.STRING + lot.getName(),
                Dao.STRING + lot.getDescription(),
                Dao.INTEGER + lot.getOwner().getId(),
                Dao.DOUBLE + lot.getStartPrice(),
                Dao.STRING + lot.getAuctionType(),
                Dao.TIMESTAMP + lot.getStartedAt().toString(),
                Dao.TIMESTAMP + lot.getFinishAt().toString()};
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement pStatement = connection.prepareStatement(INSERT)) {
            return putArgs(pStatement, args).executeUpdate() > 0;
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public boolean delete(int id) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement pStatement = connection.prepareStatement(DELETE)) {
            return putArgs(pStatement, Dao.INTEGER + id).executeUpdate() > 0;
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public Optional<Lot> findById(int id) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ResultSet rs = putArgs(ps, Dao.INTEGER + id).executeQuery();
            Lot lot;
            if (rs.next() && (lot = mapRsToObj(rs)).getId() != null)
                return Optional.of(lot);
            return Optional.empty();
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    /**
     * Update Lot .
     *
     * @param lot Lot object.
     * @return true if successfully update, otherwise false.
     * @throws CustomException if an error occurs.
     */
    @Override
    public boolean update(Lot lot) throws CustomException {
        String[] args = {
                Dao.STRING + lot.getName(),
                Dao.STRING + lot.getDescription(),
                Dao.DOUBLE + lot.getStartPrice(),
                Dao.TIMESTAMP + lot.getStartedAt().toString(),
                Dao.TIMESTAMP + lot.getFinishAt().toString(),
                Dao.INTEGER + lot.getId()};
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement pStatement = connection.prepareStatement(UPDATE)) {
            return putArgs(pStatement, args).executeUpdate() > 0;
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public List<Lot> findByOwnerId(Integer ownerId) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(FIND_BY_OWNER_ID)) {
            return getListOfLots(putArgs(ps, Dao.INTEGER + ownerId).executeQuery());
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public List<Lot> findAll() throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             Statement s = connection.createStatement()) {
            return getListOfLots(s.executeQuery(FIND_ALL));
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }


    @Override
    public List<Lot> getAllByAuctionTypeAndStatus(AuctionType auctionType, LotStatus status) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(GET_ALL_BY_AUCTION_TYPE_AND_STATUS)) {
            return getListOfLots(putArgs(ps,
                    Dao.STRING + status, Dao.STRING + auctionType).executeQuery());
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public List<Lot> getAllByAuctionType(AuctionType auctionType) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(GET_ALL_BY_AUCTION_TYPE)) {
            return getListOfLots(putArgs(ps, Dao.STRING + auctionType).executeQuery());
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public List<Lot> getAllByStatus(LotStatus status) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(GET_ALL_BY_STATUS)) {
            return getListOfLots(putArgs(ps,
                    Dao.STRING + status).executeQuery());
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public Optional<Lot> findByNameDescriptionStartedAtFinishAt(Lot lot) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(FIND_BY_NAME_DESCRIPTION_STARTED_AT_FINISH_AT)) {
            ResultSet rs = putArgs(ps,
                    Dao.STRING + lot.getName(),
                    Dao.STRING + lot.getDescription(),
                    Dao.TIMESTAMP + lot.getStartedAt(),
                    Dao.TIMESTAMP + lot.getFinishAt()).executeQuery();
            Lot lot1;
            if (rs.next() && (lot1 = mapRsToObj(rs)).getId() != null)
                return Optional.of(lot1);
            return Optional.empty();
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public boolean changeStatus(Integer id, LotStatus status) throws CustomException {
        String[] args = {
                Dao.STRING + status.name(), Dao.INTEGER + id};
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement pStatement = connection.prepareStatement(LotDAO.CHANGE_STATUS)) {
            return putArgs(pStatement, args).executeUpdate() > 0;
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    private PreparedStatement putArgs(PreparedStatement ps, String... args) throws SQLException {
        return Dao.setPSArgs(ps, args);
    }

    private List<Lot> getListOfLots(ResultSet rs) throws SQLException {
        List<Lot> lots = new ArrayList<>();
        while (rs.next())
            lots.add(mapRsToObj(rs));
        return lots;
    }

    /**
     * Converts ResultSet data To Whole Object.
     *
     * @param rs Result of executed Query.
     * @return Lot object if ResultSet is blank then return Lot without id.
     */
    private Lot mapRsToObj(ResultSet rs) throws SQLException {
        Lot lot = new Lot();
        lot.setId(rs.getInt("id"));
        lot.setName(rs.getString("name"));
        lot.setDescription(rs.getString("description"));
        User user = new User();
        user.setId(rs.getInt("owner_id"));
        lot.setOwner(user);
        lot.setStatus(LotStatus.valueOf(rs.getString("status")));
        lot.setStartPrice(rs.getDouble("start_price"));
        lot.setAuctionType(AuctionType.valueOf(rs.getString("auction_type")));
        lot.setStartedAt(rs.getTimestamp("started_at").toLocalDateTime());
        lot.setFinishAt(rs.getTimestamp("finish_at").toLocalDateTime());
        return lot;
    }
}
