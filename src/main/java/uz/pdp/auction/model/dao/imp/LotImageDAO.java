package uz.pdp.auction.model.dao.imp;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.connection.MyConnectionPool;
import uz.pdp.auction.model.dao.Dao;
import uz.pdp.auction.model.dao.LotImageDao;
import uz.pdp.auction.model.entity.Lot;
import uz.pdp.auction.model.entity.LotImage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LotImageDAO implements LotImageDao {

    private static final String INSERT = "INSERT INTO lot_image(lot_id, source)VALUES(?, ?);";
    private static final String FIND_BY_ID = "SELECT id, lot_id, source FROM lot_image WHERE id = ?;";
    private static final String FIND_BY_LOT_ID = "SELECT id, lot_id, source FROM lot_image WHERE lot_id = ?;";
    private static final String DELETE_BY_LOT_ID = "DELETE FROM lot_image WHERE lot_id = ?;";
    private static final String DELETE_BY_ID = "DELETE FROM lot_image WHERE id = ?;";

    private static LotImageDAO imageDAO;

    private LotImageDAO() {
    }

    public static LotImageDAO getInstance() {
        if (imageDAO == null)
            imageDAO = new LotImageDAO();
        return imageDAO;
    }

    @Override
    public boolean insert(LotImage lotImage) throws CustomException {
        String[] args = {Dao.INTEGER + lotImage.getLot().getId(), Dao.STRING + lotImage.getSource()};
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(INSERT)) {
            return Dao.setPSArgs(ps, args).execute();
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public boolean delete(int id) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(DELETE_BY_ID)) {
            return Dao.setPSArgs(ps, Dao.INTEGER + id).execute();
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public Optional<LotImage> findById(int id) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ResultSet rs = Dao.setPSArgs(ps, Dao.INTEGER + id).executeQuery();
            LotImage lotImage;
            if (rs.next() && (lotImage = mapRsToLotImage(rs)).getLot() != null)
                return Optional.of(lotImage);
            return Optional.empty();
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }


    /**
     * NOT Updatable.
     * @param lotImage ignored.
     * @return false.
     */
    @Override
    public boolean update(LotImage lotImage){
        return false;
    }


    @Override
    public List<LotImage> findAllByLotId(Integer lotId) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(FIND_BY_LOT_ID)) {
            ResultSet rs = Dao.setPSArgs(ps, Dao.INTEGER + lotId).executeQuery();
            List<LotImage> lotImages = new ArrayList<>();
            while (rs.next())
                lotImages.add(mapRsToLotImage(rs));
            return lotImages;
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    @Override
    public boolean deleteByLotId(Integer lotId) throws CustomException {
        try (Connection connection = MyConnectionPool.getInstance().getConnection();
             PreparedStatement ps = connection.prepareStatement(DELETE_BY_LOT_ID)) {
            return Dao.setPSArgs(ps, Dao.INTEGER + lotId).execute();
        } catch (SQLException e) {
            throw new CustomException(e.getMessage());
        }
    }

    private LotImage mapRsToLotImage(ResultSet rs)throws SQLException{
        LotImage lotImage = new LotImage();
        lotImage.setId(rs.getInt("id"));
        Lot lot = new Lot();
        lot.setId(rs.getInt("lot_id"));
        lotImage.setLot(lot);
        lotImage.setSource(rs.getString("source"));
        return lotImage;
    }
}
