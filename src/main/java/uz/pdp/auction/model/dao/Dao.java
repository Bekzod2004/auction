package uz.pdp.auction.model.dao;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.abs.AbsEntity;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Optional;

public interface Dao<T extends AbsEntity> {

    String INTEGER = "Integer - ";
    String STRING = "String - ";
    String DOUBLE = "Double - ";
    String TIMESTAMP = "Timestamp - ";
    String DATE = "0000-00-00";

    static PreparedStatement setPSArgs(PreparedStatement ps, String... args) throws SQLException {
        int index = 0;
        for (String arg : args) {
            if (arg.startsWith(INTEGER))
                ps.setInt(++index, Integer.parseInt(arg.substring(10)));
            else if (arg.startsWith(STRING))
                ps.setString(++index, arg.substring(9));
            else if (arg.startsWith(DOUBLE))
                ps.setDouble(++index, Double.parseDouble(arg.substring(9)));
            else if (arg.startsWith(TIMESTAMP))
                ps.setTimestamp(++index, Timestamp.valueOf(
                        arg.substring(TIMESTAMP.length(), TIMESTAMP.length() + DATE.length())
                                + " " + arg.substring(TIMESTAMP.length() + DATE.length() + 1)));
        }
        return ps;
    }

    boolean insert(T t) throws CustomException;

    boolean delete(int id) throws CustomException;

    Optional<T> findById(int id) throws CustomException;

    boolean update(T t) throws CustomException;
}
