package uz.pdp.auction.model.dao;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.UserRole;
import uz.pdp.auction.model.entity.enums.UserStatus;

import java.util.List;
import java.util.Optional;

public interface UserDao extends Dao<User> {

    boolean updatePassword(int id, String password) throws CustomException;
    boolean changePasswordByEmail(String email, String password) throws CustomException;

    boolean changeRole(int id, UserRole role) throws CustomException;
    boolean changeStatus(int id,UserStatus status) throws CustomException;
    Optional<User> findByEmail(String email) throws CustomException;
    Optional<User>  authenticate(String email, String password) throws CustomException;
    List<User> findAll() throws CustomException;
}
