package uz.pdp.auction.model.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import uz.pdp.auction.model.entity.Bid;
import uz.pdp.auction.model.entity.Lot;
import uz.pdp.auction.model.entity.enums.AuctionType;
import uz.pdp.auction.model.entity.enums.LotStatus;
import uz.pdp.auction.service.imp.BidServiceImp;
import uz.pdp.auction.service.imp.LotServiceImp;

import java.time.LocalDateTime;
import java.util.Optional;

@Getter
@Setter
@Builder
public class LotDto {
    private Integer id;
    private String name;
    private String description;
    private Integer owner;
    private Integer bidCount;
    private Bid winnerBid;
    private LotStatus status;
    private AuctionType auctionType;
    private double startPrice;
    private LocalDateTime startedAt;
    private LocalDateTime finishAt;


    public static LotDto mapLot(Lot lot) {
        try {
            LotDto lotDto = LotDto.builder()
                    .id(lot.getId())
                    .description(lot.getDescription())
                    .name(lot.getName())
                    .startPrice(lot.getStartPrice())
                    .startedAt(lot.getStartedAt())
                    .finishAt(lot.getFinishAt())
                    .bidCount(BidServiceImp.getInstance().findCountByLotId(lot.getId()))
                    .auctionType(lot.getAuctionType())
                    .status(lot.getStatus())
                    .owner(lot.getOwner().getId())
                    .build();
            Optional<Bid> bid = BidServiceImp.getInstance().findWinnerByLotId(lot.getId());
            if (bid.isPresent())
                lotDto.setWinnerBid(bid.get());
            else {
                Bid bid1 = new Bid();
                bid1.setAmount(lot.getStartPrice());
                lotDto.setWinnerBid(bid1);
            }

            if (lotDto.getStartedAt().isBefore(LocalDateTime.now()) && lotDto.getStatus().equals(LotStatus.NEW)) {
                LotServiceImp.getInstance().changeStatus(lot.getId(), LotStatus.SUSPENDED);
                lotDto.setStatus(LotStatus.SUSPENDED);
            }
            if (lotDto.getFinishAt().isBefore(LocalDateTime.now()) && Math.abs(lotDto.getStartPrice() - lotDto.getWinnerBid().getAmount()) < 1) {
                LotServiceImp.getInstance().changeStatus(lot.getId(), LotStatus.SUSPENDED);
                lotDto.setStatus(LotStatus.SUSPENDED);
            }
            if (lotDto.getFinishAt().isBefore(LocalDateTime.now()) && Math.abs(lotDto.getStartPrice() - lotDto.getWinnerBid().getAmount()) > 1) {
                LotServiceImp.getInstance().changeStatus(lot.getId(), LotStatus.SOLD);
                lotDto.setStatus(LotStatus.SOLD);
            }
            return lotDto;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}


