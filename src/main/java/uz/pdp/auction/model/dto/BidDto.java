package uz.pdp.auction.model.dto;

import lombok.*;
import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.Bid;
import uz.pdp.auction.model.entity.enums.AuctionType;
import uz.pdp.auction.model.entity.enums.LotStatus;
import uz.pdp.auction.service.imp.BidServiceImp;
import uz.pdp.auction.service.imp.LotServiceImp;

import java.time.LocalDateTime;
import java.util.Optional;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BidDto {
    private Integer id;
    private Integer owner;
    private Integer lot;
    private String lotName;
    private double amount;
    private boolean isWinner;
    private LocalDateTime updatedAt;
    private AuctionType auctionType;
    private LotStatus lotStatus;

    private LocalDateTime lotFinishTime = LocalDateTime.now();


    public static BidDto mapBid(Bid bid) {
        BidDto bidDto = new BidDto();
        bidDto.id = bid.getId();
        bidDto.amount = bid.getAmount();
        bidDto.owner = bid.getOwner().getId();
        bidDto.lot = bid.getLot().getId();
        bidDto.lotName = bid.getLot().getName();
        bidDto.updatedAt = bid.getUpdatedAt();

        try {
            LotServiceImp.getInstance().findById(bid.getLot().getId()).ifPresent(lot -> {
                bidDto.lotFinishTime = lot.getFinishAt();
                bidDto.auctionType = lot.getAuctionType();
                bidDto.lotStatus = lot.getStatus();
            });
            Optional<Bid> bid1 = BidServiceImp.getInstance().findWinnerByLotId(bid.getLot().getId());
            bid1.ifPresent(value -> bidDto.isWinner = value.getId().equals(bid.getId()));
        } catch (CustomException e) {
            throw new RuntimeException(e);
        }
        return bidDto;
    }
}
