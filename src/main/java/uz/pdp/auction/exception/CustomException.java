package uz.pdp.auction.exception;

public class CustomException extends Exception{
    public CustomException(String msg){
        super(msg);
    }
    public CustomException(){}
    public CustomException(Throwable cause){
        super(cause);
    }
    public CustomException(String msg,Throwable cause){
        super(msg,cause);
    }
}
