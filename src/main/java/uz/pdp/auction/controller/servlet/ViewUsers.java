package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.UserRole;
import uz.pdp.auction.service.imp.UserServiceImp;
import uz.pdp.auction.util.Utils;

import java.io.IOException;
import java.util.Comparator;
import java.util.stream.Collectors;

@WebServlet(name = "ViewUsers", urlPatterns = "/users")
public class ViewUsers extends HttpServlet {
    private final UserServiceImp userServiceImp = UserServiceImp.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            User user = (User) req.getSession().getAttribute("user");
            if (user == null) {
                resp.sendRedirect("/login");
                return;
            }
            req.setAttribute(
                    "users",
                    userServiceImp.findAll()
                            .stream()
                            .filter(u -> check(user, u))
                            .sorted(Comparator.comparing(User::getEmail))
                            .collect(Collectors.toList()));
        } catch (CustomException e) {
            req.setAttribute("error", "something went wrong");
        }
        req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "users.jsp").forward(req, resp);
    }

    private boolean check(User user, User u) {
        switch (user.getRole()) {
            case ADMIN:
                return u.getRole().equals(UserRole.USER);
            case SUPER_ADMIN:
                return !u.getRole().equals(UserRole.SUPER_ADMIN);
            default:
                return false;
        }
    }
}
