package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.Lot;
import uz.pdp.auction.model.entity.LotImage;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.service.imp.LotImageServiceImp;
import uz.pdp.auction.service.imp.LotServiceImp;
import uz.pdp.auction.util.Utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;

@MultipartConfig
@WebServlet(name = "UpdateLot", urlPatterns = "/update-lot")
public class UpdateLot extends HttpServlet {
    private final LotServiceImp lotServiceImp = LotServiceImp.getInstance();
    private final LotImageServiceImp lotImageServiceImp = LotImageServiceImp.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id;
        Lot lot;
        try {
            id = Integer.parseInt(req.getParameter("id"));
            lot = lotServiceImp.findById(id).orElseThrow(CustomException::new);
            req.setAttribute("lot",lot);
        } catch (Exception ignored) {}
        req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "update-lot.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id;
        Lot lot;
        User user = (User) req.getSession().getAttribute("user");
        try {
            id = Integer.valueOf(req.getParameter("id"));
            lot = lotServiceImp.findById(id).orElseThrow(CustomException::new);
            if(!lot.getOwner().getId().equals(user.getId()))
                throw new RuntimeException("Bad credentials");
            Lot newLot = OfferLot.getFromParts(req);
            if (newLot.getName() != null && !newLot.getName().isEmpty())
                lot.setName(newLot.getName());
            if(newLot.getDescription() != null && !newLot.getDescription().isEmpty())
                lot.setDescription(newLot.getDescription());
            if(newLot.getStartPrice() >0)
                lot.setStartPrice(newLot.getStartPrice());
            if(newLot.getStartedAt() != null)
                lot.setStartedAt(newLot.getStartedAt());
            if(newLot.getFinishAt() != null)
                lot.setFinishAt(newLot.getFinishAt());
            lotServiceImp.update(lot);
            if (req.getPart("image") != null && req.getPart("image").getInputStream().readAllBytes().length > 0) {
                Optional<LotImage> lotImage = lotImageServiceImp.findAllByLotId(id).stream().findFirst();
                lotImage.ifPresent(lotImage1 -> {
                    try {
                        lotImageServiceImp.delete(lotImage1.getId());
                    } catch (CustomException e) {
                        throw new RuntimeException(e);
                    }
                });
                Part part = req.getPart("image");
                lotImageServiceImp.uploadImage(id, id + part.getSubmittedFileName().replace(" ", ""));
                try (FileOutputStream os = new FileOutputStream(Utils.IMAGES_PATH + id + part.getSubmittedFileName().replace(" ", ""))) {
                    os.write(part.getInputStream().readAllBytes());
                }
            }
            resp.sendRedirect("lot?id=" + id);
        } catch (Exception e) {
            req.setAttribute("error",e.getMessage());
            req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "update-lot.jsp").forward(req,resp);
        }
    }
}
