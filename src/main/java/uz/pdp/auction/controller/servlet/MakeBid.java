package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.Lot;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.service.imp.BidServiceImp;
import uz.pdp.auction.service.imp.LotServiceImp;
import uz.pdp.auction.util.Utils;

import java.io.IOException;

@WebServlet(name = "MakeBid", urlPatterns = "/bid")
public class MakeBid extends HttpServlet {
    private final BidServiceImp bidServiceImp = BidServiceImp.getInstance();
    private final LotServiceImp lotServiceImp = LotServiceImp.getInstance();


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        try {
            Lot lot = lotServiceImp.findById(Integer.valueOf(req.getParameter("lot-id"))).orElseThrow(() -> new CustomException("lot id not found"));
            if (bidServiceImp.create(user.getId(), lot.getId(), Double.parseDouble(req.getParameter("amount"))))
                resp.sendRedirect("/my-bids");
            else resp.sendRedirect("/home");
        } catch (Exception e) {
            req.setAttribute("error", e.getMessage());
            req.getRequestDispatcher(Utils.ERRORS_ROOT_PATH + "error.jsp").forward(req,resp);
        }
    }
}
