package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.dao.imp.LotDAO;
import uz.pdp.auction.model.dto.LotDto;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.LotStatus;
import uz.pdp.auction.model.entity.enums.UserRole;
import uz.pdp.auction.util.Utils;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "Home", value = "/home")
public class Home extends HttpServlet {
    private final LotDAO lotDAO = LotDAO.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            User user = (User) req.getSession().getAttribute("user");
            List<LotDto> lotDTOs = lotDAO.findAll()
                    .stream()
                    .map(LotDto::mapLot)
                    .filter(lotdto -> checkLot(user, lotdto))
                    .sorted(Comparator.comparing(LotDto::getName))
                    .collect(Collectors.toList());
            req.setAttribute("lotDTOs", lotDTOs);
        } catch (CustomException e) {
            throw new RuntimeException(e);
        }
        req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "home.jsp").forward(req, resp);
    }
    private boolean checkLot(User user, LotDto lotDto) {
        return (user != null && user.getRole().equals(UserRole.USER) && !lotDto.getOwner().equals(user.getId()) && lotDto.getStatus().equals(LotStatus.ACTIVE))
                || ( user == null && lotDto.getStatus().equals(LotStatus.ACTIVE))
                || (user != null && (user.getRole().equals(UserRole.ADMIN) || user.getRole().equals(UserRole.SUPER_ADMIN)));
    }
}
