package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.service.imp.UserServiceImp;
import uz.pdp.auction.util.Utils;
import uz.pdp.auction.util.mail.MailSender;

import java.io.IOException;
import java.util.Random;

@WebServlet(name = "RefactorPassword", urlPatterns = "/password-recovery")
public class RefactorPassword extends HttpServlet {
    private final UserServiceImp userServiceImp = UserServiceImp.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            if (userServiceImp.findByEmail(req.getParameter("email")).isPresent()) {
                Integer code = new Random().nextInt(8999) + 1000;
                req.getSession().setAttribute("password-recovery-code", code);
                req.getSession().setAttribute("email", req.getParameter("email"));
                if (MailSender.getInstance().sendMail(req.getParameter("email"), "Password Recovery Confirmation Code", "Your Confirmation Code - " + code)) {
                    req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "recover-password.jsp").forward(req, resp);
                } else {
                    req.setAttribute("error", "Confirmation code could not be sent to this email");
                    req.getRequestDispatcher(Utils.ERRORS_ROOT_PATH + "error.jsp").forward(req, resp);
                }
            } else {
                req.setAttribute("error", "This email is invalid or Not registered before");
                req.getRequestDispatcher(Utils.ERRORS_ROOT_PATH + "error.jsp").forward(req, resp);
            }
        } catch (Exception e) {
            req.setAttribute("error", e.getMessage());
            req.getRequestDispatcher(Utils.ERRORS_ROOT_PATH + "error.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer code = (Integer) req.getSession().getAttribute("password-recovery-code");
        Integer codeInSession = Integer.valueOf(req.getParameter("code"));
        if (code == null || !code.equals(codeInSession)) {
            req.setAttribute("error", "Confirmation Code is invalid to process");
            req.getRequestDispatcher(Utils.ERRORS_ROOT_PATH + "error.jsp").forward(req, resp);
        } else if (!req.getParameter("password").equals(req.getParameter("pre-password"))) {
            req.setAttribute("error", "Passwords did not match");
            req.getRequestDispatcher(Utils.ERRORS_ROOT_PATH + "error.jsp").forward(req, resp);
        } else {
            try {
                if (userServiceImp.changePasswordByEmail((String) req.getSession().getAttribute("email"), req.getParameter("password")))
                    req.getSession().setAttribute("user", userServiceImp.authenticate((String) req.getSession().getAttribute("email"), req.getParameter("password")).orElseThrow(() -> new RuntimeException("Something went wrong")));
                resp.sendRedirect("/home");
            } catch (Exception e) {
                req.setAttribute("error", e.getMessage());
                req.getRequestDispatcher(Utils.ERRORS_ROOT_PATH + "error.jsp").forward(req, resp);
            }
        }
    }
}
