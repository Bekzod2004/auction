package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.UserRole;
import uz.pdp.auction.service.imp.UserServiceImp;

import java.io.IOException;

@WebServlet(name = "ChangeUserRole", urlPatterns = "/user-role")
public class ChangeUserRole extends HttpServlet {
    private final UserServiceImp userServiceImp = UserServiceImp.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id;
        UserRole role;
        try {
            id = Integer.parseInt(req.getParameter("id"));
            role = UserRole.valueOf(req.getParameter("role"));
            if (role.equals(UserRole.SUPER_ADMIN))
                throw new Exception("User role could not be SUPER_ADMIN");
            if (!userServiceImp.changeRole(id, role))
                req.setAttribute("error", "Role could not be changed");
        } catch (Exception e) {
            req.setAttribute("error", e.getMessage());
        }
        User user = (User) req.getSession().getAttribute("user");
        if (user != null && !user.getRole().equals(UserRole.USER))
            resp.sendRedirect("/users");
        else
            resp.sendRedirect("/bad-credentials");
    }
}
