package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.model.entity.Lot;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.UserRole;
import uz.pdp.auction.service.imp.LotServiceImp;

import java.io.IOException;

@WebServlet(name = "DeleteLot", urlPatterns = "/delete-lot")
public class DeleteLot extends HttpServlet {

    private final LotServiceImp lotServiceImp = LotServiceImp.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id;
        User user = (User) req.getSession().getAttribute("user");
        try {
            id = Integer.parseInt(req.getParameter("id"));
            Lot lot = lotServiceImp.findById(id).orElseThrow(RuntimeException::new);
            if (!user.getRole().equals(UserRole.USER) || lot.getOwner().getId().equals(user.getId()))
                lotServiceImp.delete(id);
            else resp.sendRedirect("/lot?id=" + id);
        } catch (Exception ignored) {
        }
        resp.sendRedirect("/home");
    }
}
