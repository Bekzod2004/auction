package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.LotImage;
import uz.pdp.auction.service.imp.LotImageServiceImp;
import uz.pdp.auction.util.ImagesRootPath;

import java.io.FileInputStream;
import java.io.IOException;

@WebServlet(name = "GetImage",urlPatterns = "/image")
public class GetImage extends HttpServlet {

    private final LotImageServiceImp lotImageServiceImp = LotImageServiceImp.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int lotId;
        try {
            lotId = Integer.parseInt(req.getParameter("lot-id"));
            LotImage lotImage = lotImageServiceImp.findAllByLotId(lotId).stream().findFirst().orElseThrow(CustomException::new);
            try(FileInputStream is = new FileInputStream(ImagesRootPath.imagesRootPath() + lotImage.getSource())) {
                resp.getOutputStream().write(is.readAllBytes());
            }
        }catch (Exception ignored){
            try(FileInputStream is = new FileInputStream(ImagesRootPath.imagesRootPath() + "no-image.png")) {
                resp.getOutputStream().write(is.readAllBytes());
            }
        }

    }
}
