package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.dto.BidDto;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.service.imp.BidServiceImp;
import uz.pdp.auction.util.Utils;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "MyBids", urlPatterns = "/my-bids")
public class MyBids extends HttpServlet {

    private final BidServiceImp bidServiceImp = BidServiceImp.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");
        if (user != null) {
            try {
                List<BidDto> bidDTOs = bidServiceImp.findByOwnerId(user.getId())
                        .stream()
                        .map(BidDto::mapBid)
                        .sorted((b1, b2) -> b2.getUpdatedAt().compareTo(b1.getUpdatedAt()))
                        .collect(Collectors.toList());
                req.setAttribute("bidDTOs", bidDTOs);
            } catch (CustomException ignored) {
            }
        }
        req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "my-bids.jsp").forward(req, resp);
    }
}
