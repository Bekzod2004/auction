package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.util.Utils;

import java.io.IOException;


@WebServlet(name="ErrorServlet",urlPatterns = {"/error"})
public class ErrorResponse extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(Utils.ERRORS_ROOT_PATH + "error.jsp").forward(req,resp);
    }
}
