package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.service.imp.UserServiceImp;
import uz.pdp.auction.util.Utils;

import java.io.IOException;

@WebServlet(name = "ChangePassword", urlPatterns = "/change-password")
public class ChangePassword extends HttpServlet {
    private final UserServiceImp userServiceImp = UserServiceImp.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "change-password.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            if (!req.getParameter("password").equals(req.getParameter("pre-password"))) {
                req.setAttribute("error", "Password should match to Pre password");
                req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "change-password.jsp").forward(req, resp);
            }
            if (userServiceImp.authenticate(((User) req.getSession().getAttribute("user")).getEmail(), req.getParameter("old-password")).isPresent()) {
                if (userServiceImp.changePassword(((User) req.getSession().getAttribute("user")).getId(), req.getParameter("password")))
                    resp.sendRedirect("/home");
                else {
                    req.setAttribute("error", "Something went wrong");
                    req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "change-password.jsp").forward(req, resp);
                }
            } else {
                req.setAttribute("error", "Old Password is incorrect or user is not registered");
                req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "change-password.jsp").forward(req, resp);
            }
        } catch (CustomException e) {
            req.setAttribute("error", e.getMessage());
            req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "change-password.jsp").forward(req, resp);
        }
    }
}
