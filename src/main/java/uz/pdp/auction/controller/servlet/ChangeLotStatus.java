package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.LotStatus;
import uz.pdp.auction.model.entity.enums.UserRole;
import uz.pdp.auction.service.imp.LotServiceImp;

import java.io.IOException;

@WebServlet(name = "ChangeLotStatus", urlPatterns = "/lot-status")
public class ChangeLotStatus extends HttpServlet {
    private final LotServiceImp lotServiceImp = LotServiceImp.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = null;
        LotStatus status;
        User user = (User) req.getSession().getAttribute("user");
        try {
            if(user!=null && !user.getRole().equals(UserRole.USER)) {
                id = Integer.valueOf(req.getParameter("id"));
                status = LotStatus.valueOf(req.getParameter("lot-status"));
                lotServiceImp.changeStatus(id, status);
            }
        } catch (Exception ignored) {}
        resp.sendRedirect(id != null ? "/lot?id=" + id : "/home");
    }
}
