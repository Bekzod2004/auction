package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.dto.LotDto;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.service.imp.LotServiceImp;
import uz.pdp.auction.util.Utils;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "MyLots", urlPatterns = "/my-lots")
public class MyLots extends HttpServlet {
    private final LotServiceImp lotServiceImp = LotServiceImp.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");
        if (user != null) {
            try {
                List<LotDto> lotDTOs = lotServiceImp.findByOwnerId(user.getId()).stream()
                        .map(LotDto::mapLot)
                        .collect(Collectors.toList());
                req.setAttribute("lotDTOs", lotDTOs);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "my-lots.jsp").forward(req, resp);
    }
}
