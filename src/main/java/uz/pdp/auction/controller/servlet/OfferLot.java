package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.Lot;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.AuctionType;
import uz.pdp.auction.model.entity.enums.LotStatus;
import uz.pdp.auction.service.imp.LotImageServiceImp;
import uz.pdp.auction.service.imp.LotServiceImp;
import uz.pdp.auction.util.ImagesRootPath;
import uz.pdp.auction.util.Utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.LocalTime;

@MultipartConfig
@WebServlet(name = "OfferLot", urlPatterns = "/offer")
public class OfferLot extends HttpServlet {
    LotServiceImp lotServiceImp = LotServiceImp.getInstance();
    LotImageServiceImp lotImageServiceImp = LotImageServiceImp.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "lot-offer.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Lot lot = lotServiceImp.offer(getFromParts(req)).orElseThrow(() -> new CustomException("Lot could not be saved"));
            if (req.getPart("image") != null) {
                Part part = req.getPart("image");
                lotImageServiceImp.uploadImage(lot.getId(), lot.getId() + part.getSubmittedFileName().replace(" ", ""));
                try (FileOutputStream os = new FileOutputStream(ImagesRootPath.imagesRootPath() + lot.getId() + part.getSubmittedFileName().replace(" ", ""))) {
                    os.write(part.getInputStream().readAllBytes());
                }
            }
            resp.sendRedirect("/my-lots");
        } catch (Exception e) {
            req.setAttribute("error", e.getMessage());
            req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "lot-offer.jsp").forward(req, resp);
        }
    }

    public static Lot getFromParts(HttpServletRequest req) throws IOException, ServletException {
        Lot lot = new Lot();
        InputStream is;
        Part part;
        StringBuilder builder;
        part = req.getPart("name");
        if (part != null) {
            is = part.getInputStream();
            builder = new StringBuilder();
            for (byte b : is.readAllBytes())
                builder.append((char) b);
            lot.setName(builder.toString());
        }

        part = req.getPart("description");
        if(part!= null) {
            is = part.getInputStream();
            builder = new StringBuilder();
            for (byte b : is.readAllBytes())
                builder.append((char) b);
            lot.setDescription(builder.toString());
        }

        part = req.getPart("auction_type");
        if(part != null) {
            is = part.getInputStream();
            builder = new StringBuilder();
            for (byte b : is.readAllBytes())
                builder.append((char) b);
            lot.setAuctionType(AuctionType.valueOf(builder.toString()));
            lot.setStatus(LotStatus.NEW);
        }

        part = req.getPart("start_price");
        if(part != null) {
            is = part.getInputStream();
            builder = new StringBuilder();
            for (byte b : is.readAllBytes())
                builder.append((char) b);
            lot.setStartPrice(Double.parseDouble(String.valueOf(builder)));
        }

        part = req.getPart("started_at");
        if(part != null) {
            is = part.getInputStream();
            lot.setStartedAt(parseBytesToLDT(is.readAllBytes()));
        }

        part = req.getPart("finish_at");
        if(part != null) {
            is = part.getInputStream();
            lot.setFinishAt(parseBytesToLDT(is.readAllBytes()));
        }

        lot.setOwner((User) req.getSession().getAttribute("user"));
        return lot;
    }

    private static LocalDateTime parseBytesToLDT(byte[] ar) {
        if (ar.length > 1) {
            StringBuilder builder = new StringBuilder(20);
            for (byte b : ar) {
                if (b != 'T')
                    builder.append((char) b);
                else
                    builder.append(' ');
            }
            builder.append(":00.000000");
            LocalDateTime localDateTime = Timestamp.valueOf(builder.toString()).toLocalDateTime();
            return LocalDateTime.of(localDateTime.toLocalDate(), LocalTime.of(localDateTime.getHour(), localDateTime.getMinute(), 41, 123456));
        } else
            throw new RuntimeException("DateTime is not valid");
    }
}
