package uz.pdp.auction.controller.servlet;

import java.io.*;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.service.imp.UserServiceImp;
import uz.pdp.auction.util.Utils;

@WebServlet(name = "Login", value = "/login")
public class Login extends HttpServlet {
    private final UserServiceImp userService = UserServiceImp.getInstance();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "login.jsp").forward(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            User user = userService.authenticate(request.getParameter("email"), request.getParameter("password")).orElseThrow(() -> {
                response.setStatus(404);
                throw new RuntimeException("Password or username is incorrect");
            });
            request.getSession().removeAttribute("user");
            request.getSession().setAttribute("user", user);
            response.sendRedirect("/home");
        } catch (Exception e) {
            request.setAttribute("error", e.getMessage());
            request.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "login.jsp").forward(request, response);
        }
    }

}