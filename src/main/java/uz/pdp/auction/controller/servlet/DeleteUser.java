package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.service.imp.UserServiceImp;

import java.io.IOException;

@WebServlet(name = "DeleteUser", urlPatterns = "/delete-user")
public class DeleteUser extends HttpServlet {
    private final UserServiceImp userServiceImp = UserServiceImp.getInstance();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id;
        try {
            id = Integer.parseInt(req.getParameter("id"));
            userServiceImp.delete(id);
            resp.sendRedirect("/users");
        }catch (Exception e){
            resp.setHeader("error",e.getMessage());
            resp.sendRedirect("/error");
        }
    }

}
