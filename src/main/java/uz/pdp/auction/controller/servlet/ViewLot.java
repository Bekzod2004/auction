package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.dto.LotDto;
import uz.pdp.auction.model.entity.Bid;
import uz.pdp.auction.model.entity.Lot;
import uz.pdp.auction.service.imp.BidServiceImp;
import uz.pdp.auction.service.imp.LotImageServiceImp;
import uz.pdp.auction.service.imp.LotServiceImp;
import uz.pdp.auction.util.Utils;

import java.io.IOException;

@WebServlet(name = "ViewLot",urlPatterns = "/lot")
public class ViewLot extends HttpServlet {
    private final LotServiceImp lotServiceImp = LotServiceImp.getInstance();
    private final LotImageServiceImp lotImageServiceImp = LotImageServiceImp.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer lotId = Integer.valueOf(req.getParameter("id"));
        try {
            LotDto lot = LotDto.mapLot(lotServiceImp.findById(lotId).orElseThrow(() -> new CustomException("Lot id not found")));
            req.setAttribute("lot", lot);
            lotImageServiceImp.findAllByLotId(lotId)
                    .stream()
                    .findFirst()
                    .ifPresent(image -> req.setAttribute("image", image.getSource()));
            req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "lot.jsp").forward(req, resp);
        } catch (CustomException e) {
            req.setAttribute("error", e.getMessage());
            req.getRequestDispatcher(Utils.ERRORS_ROOT_PATH + "error.jsp").forward(req, resp);
        }
    }
}
