package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.UserRole;
import uz.pdp.auction.model.entity.enums.UserStatus;
import uz.pdp.auction.service.imp.UserServiceImp;

import java.io.IOException;

@WebServlet(name = "ChangeUserStatus", urlPatterns = "/user-status")
public class ChangeUserStatus extends HttpServlet {
    private final UserServiceImp userServiceImp = UserServiceImp.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id;
        UserStatus status;
        User user = (User) req.getSession().getAttribute("user");
        try {

            id = Integer.parseInt(req.getParameter("id"));
            status = UserStatus.valueOf(req.getParameter("status"));
            if (check(id, status, user))
                userServiceImp.changeStatus(id, status);
            else
                resp.sendRedirect("/error");
        } catch (Exception e) {
            req.setAttribute("error", e.getMessage());
        }
        if (!user.getRole().equals(UserRole.USER))
            resp.sendRedirect("/users");
        else resp.sendRedirect("/bad-credentials");
    }

    private boolean check(int id, UserStatus status, User user) throws CustomException {
        if (user == null || id == user.getId())
            throw new RuntimeException("User can not change his own status");
        User user1 = userServiceImp.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
        if (user.getRole().equals(UserRole.SUPER_ADMIN))
            return !user1.getRole().equals(UserRole.SUPER_ADMIN);
        if (user.getRole().equals(UserRole.ADMIN))
            return user1.getRole().equals(UserRole.USER);
        return false;
    }
}
