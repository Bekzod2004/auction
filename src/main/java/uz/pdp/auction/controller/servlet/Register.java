package uz.pdp.auction.controller.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.service.imp.UserServiceImp;
import uz.pdp.auction.util.Utils;

import java.io.IOException;

@WebServlet(name = "Register", value = "/register")
public class Register extends HttpServlet {
    private final UserServiceImp userServiceImp = UserServiceImp.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "register.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            if (!req.getParameter("password").equals(req.getParameter("pre-password"))) {
                throw new CustomException("Password should match to the Pre Password!");
            } else if (userServiceImp.findByEmail(req.getParameter("email")).isPresent()) {
                throw new CustomException("This user is already registered!");
            } else {
                if (userServiceImp.signUp(req.getParameter("email"), req.getParameter("password"))) {
                    req.getSession().setAttribute("user", userServiceImp.authenticate(req.getParameter("email"), req.getParameter("password")).orElseThrow(() -> {
                        resp.setStatus(403);
                        throw new RuntimeException("User could not be registered");
                    }));
                    resp.sendRedirect("home");
                }
            }
        } catch (Exception e) {
            req.setAttribute("error", e.getMessage());
            req.getRequestDispatcher(Utils.PAGES_ROOT_PATH + "register.jsp").forward(req, resp);
        }
    }
}
