package uz.pdp.auction.controller.filter;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.UserRole;

import java.io.IOException;

@WebFilter(filterName = "MyBidsAndLotsFilter",urlPatterns = {"/my-bids","/my-lots"})
public class MyBidsAndLotsFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        User user = (User) req.getSession().getAttribute("user");
        if (user == null)
            ((HttpServletResponse) servletResponse).sendRedirect("/login");
        else if (!user.getRole().equals(UserRole.USER))
            ((HttpServletResponse) servletResponse).sendRedirect("/bad-credentials");
        else
            filterChain.doFilter(req, servletResponse);
    }
}
