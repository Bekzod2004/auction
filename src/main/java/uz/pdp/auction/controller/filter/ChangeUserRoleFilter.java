package uz.pdp.auction.controller.filter;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.UserRole;

import java.io.IOException;

@WebFilter(filterName = "ChangeUserRoleFilter", urlPatterns = "/user-role")
public class ChangeUserRoleFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        User user = (User) ((HttpServletRequest)servletRequest).getSession().getAttribute("user");
        if(user!= null && user.getRole().equals(UserRole.SUPER_ADMIN))
            filterChain.doFilter(servletRequest,servletResponse);
        else
            ((HttpServletResponse)servletResponse).sendRedirect("/bad-credentials");
    }
}
