package uz.pdp.auction.controller.filter;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.model.entity.User;

import java.io.IOException;

@WebFilter(filterName = "DeleteLotFilter", urlPatterns = "/delete-lot")
public class DeleteLotFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        User user = (User) ((HttpServletRequest)servletRequest).getSession().getAttribute("user");
        if(user != null)
            filterChain.doFilter(servletRequest,servletResponse);
        else
            ((HttpServletResponse)servletResponse).sendRedirect("/login");
    }
}
