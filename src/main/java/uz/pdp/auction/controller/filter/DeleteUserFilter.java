package uz.pdp.auction.controller.filter;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.UserRole;

import java.io.IOException;

@WebFilter(filterName = "DeleteUserFilter", urlPatterns = "/delete-user")
public class DeleteUserFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException{
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        User user = (User) req.getSession().getAttribute("user");
        if (user == null || user.getRole().equals(UserRole.USER))
            ((HttpServletResponse) servletResponse).sendRedirect("/bad-credentials");
        else
            try {
                int id = Integer.parseInt(req.getParameter("id"));
                if (user.getId().equals(id))
                    ((HttpServletResponse) servletResponse).sendRedirect("/bad-credentials");
                else
                    filterChain.doFilter(req, servletResponse);
            } catch (Exception e) {
                ((HttpServletResponse) servletResponse).sendRedirect("/error");
            }
    }
}
