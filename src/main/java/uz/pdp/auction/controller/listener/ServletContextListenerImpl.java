package uz.pdp.auction.controller.listener;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.connection.MyConnectionPool;
import uz.pdp.auction.util.ImagesRootPath;

@WebListener
public class ServletContextListenerImpl implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            MyConnectionPool.getInstance();
        } catch (CustomException e) {
            throw new RuntimeException(e);
        }
        System.out.println("IMages root path : " + ImagesRootPath.imagesRootPath());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            MyConnectionPool.getInstance().destroyPool();
        } catch (CustomException e) {
            throw new RuntimeException(e);
        }
    }
}
