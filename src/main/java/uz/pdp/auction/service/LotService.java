package uz.pdp.auction.service;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.Lot;
import uz.pdp.auction.model.entity.enums.AuctionType;
import uz.pdp.auction.model.entity.enums.LotStatus;

import java.util.List;
import java.util.Optional;

public interface LotService {

    /**
     * Adds new Lot Object.
     *
     * @param lot new Lot obj without id.
     * @return Optional lot if new Lot successfully added to db, otherwise Optional.empty.
     * @throws CustomException if an error occurs.
     */
    Optional<Lot> offer(Lot lot) throws CustomException;

    /**
     * Find by id.
     *
     * @param id Lot id.
     * @return Lot object If exists with this id, otherwise Optional.empty();
     * @throws CustomException if an error occurs.
     */
    Optional<Lot> findById(Integer id) throws CustomException;

    /**
     * Find Lots by their owners' ids.
     *
     * @param ownerId Lot's ownerId.
     * @return List of lots if exists with this ownerId, otherwise empty List.
     * @throws CustomException if an error occurs.
     */
    List<Lot> findByOwnerId(Integer ownerId) throws CustomException;

    List<Lot> getAllByAuctionTypeAndStatus(AuctionType auctionType, LotStatus status) throws CustomException;
    /**
     * Updates LotStatus of lot with given id.
     *
     * @param id     Lot id.
     * @param status LotStatus obj in (NEW, PROCESSING,  ACTIVE, SOLD,  WENT_OFF, BLOCKED).
     * @return true if there is a lot with this id and updated, otherwise false.
     * @throws CustomException if an error occurs.
     */
    boolean changeStatus(Integer id, LotStatus status) throws CustomException;


    boolean update(Lot lot) throws CustomException;

    /**
     * Delete Lot.
     *
     * @param id Lot id.
     * @throws CustomException if there is no Lot with this id or another error occurs.
     */
    void delete(Integer id) throws CustomException;
}
