package uz.pdp.auction.service;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.LotImage;

import java.util.List;
import java.util.Optional;

public interface LotImageService {

    /**
     * Upload Image for Item.
     *
     * @param itemId Item id.
     * @param source uploaded Image url.
     * @return true if there is an Item with this id and image url can be uploaded, otherwise false.
     * @throws CustomException if an error occurs.
     */
    boolean uploadImage(Integer itemId, String source) throws CustomException;

    /**
     * Find All Images of an Item.
     *
     * @param itemId Item id.
     * @return List of ItemImage objs if images exist with this item_id. otherwise null.
     * @throws CustomException if an error occurs.
     */
    List<LotImage> findAllByLotId(Integer itemId) throws CustomException;

    /**
     * Find Image by id.
     *
     * @param id Image id.
     * @return Image if exists with this id, otherwise Optional.empty().
     * @throws CustomException if an error occurs.
     */
    Optional<LotImage> findById(Integer id) throws CustomException;

    /**
     * Delete Image.
     *
     * @param id Image id.
     * @throws CustomException if there is no image with this id or another error occurs.
     */
    void delete(Integer id) throws CustomException;
}
