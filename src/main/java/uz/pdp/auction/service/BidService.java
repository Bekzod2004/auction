package uz.pdp.auction.service;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.Bid;

import java.util.List;
import java.util.Optional;

public interface BidService {
    boolean create(Integer ownerId, Integer lotId, double amount) throws CustomException;
    List<Bid> findByLotId(Integer lotId) throws CustomException;
    List<Bid> findByOwnerId(Integer ownerId) throws CustomException;
    Integer findCountByLotId(Integer lotId)throws CustomException;

    Optional<Bid> findWinnerByLotId(Integer lotId) throws CustomException;
}
