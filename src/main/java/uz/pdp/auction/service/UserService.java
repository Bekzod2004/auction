package uz.pdp.auction.service;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.UserRole;
import uz.pdp.auction.model.entity.enums.UserStatus;

import java.util.List;
import java.util.Optional;

public interface UserService {

    /**
     * Registers as a new User.
     *
     * @param email    User email.
     * @param password User password.
     * @return true if there is no user with this email and password is valid, otherwise false.
     * @throws CustomException If an error occurs.
     */
    boolean signUp(String email, String password) throws CustomException;

    /**
     * Authenticate  user.
     *
     * @param email    User email.
     * @param password User password.
     * @return User without password if there is a user with this email and password, otherwise Optional.empty().
     * @throws CustomException If an error occurs.
     */
    Optional<User> authenticate(String email, String password) throws CustomException;

    /**
     * Finds  user with id.
     *
     * @param id User id.
     * @return User without password if there is a user with this id, otherwise Optional.empty().
     * @throws CustomException If an error occurs.
     */
    Optional<User> findById(Integer id) throws CustomException;

    /**
     * Get users.
     * @return List of all users without passwords.
     * @throws CustomException If an error occurs.
     */
    List<User> findAll() throws CustomException;

    /**
     * Finds  user with email.
     *
     * @param email User email.
     * @return User without password if there is a user with this email, otherwise Optional.empty().
     * @throws CustomException If an error occurs.
     */
    Optional<User> findByEmail(String email) throws CustomException;

    /**
     * Updates user's password by id.
     *
     * @param id       User's id.
     * @param password New password.
     * @return true.
     * @throws CustomException If an error occurs.
     */
    boolean changePassword(Integer id, String password) throws CustomException;
    /**
     * Updates user's password by email.
     *
     * @param email       User's email.
     * @param password New password.
     * @return true.
     * @throws CustomException If an error occurs.
     */
    boolean changePasswordByEmail(String email, String password) throws CustomException;

    /**
     * Update user role and status.
     *
     * @param id   User id.
     * @param role User role: ADMIN or USER.
     * @return true, if user exists and was updated, otherwise false.
     * @throws CustomException if an error occurs.
     */
    boolean changeRole(Integer id, UserRole role) throws CustomException;

    /**
     * Update user role and status.
     *
     * @param id     User id.
     * @param status User status: ACTIVE or BLOCKED.
     * @return true, if user exists and was updated, otherwise false.
     * @throws CustomException if an error occurs.
     */
    boolean changeStatus(Integer id, UserStatus status) throws CustomException;

    void delete(Integer id)throws CustomException;
}
