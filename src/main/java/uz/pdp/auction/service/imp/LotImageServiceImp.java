package uz.pdp.auction.service.imp;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.dao.imp.LotImageDAO;
import uz.pdp.auction.model.entity.Lot;
import uz.pdp.auction.model.entity.LotImage;
import uz.pdp.auction.service.LotImageService;

import java.util.List;
import java.util.Optional;

public class LotImageServiceImp implements LotImageService {
    private final LotImageDAO imageDAO = LotImageDAO.getInstance();
    private static LotImageServiceImp imp;

    private LotImageServiceImp() {
    }

    public static LotImageServiceImp getInstance() {
        if (imp == null)
            imp = new LotImageServiceImp();
        return imp;
    }

    @Override
    public boolean uploadImage(Integer lotId, String source) throws CustomException {
        if (lotId == null)
            throw new CustomException("lotId is null");
        if (source == null)
            throw new CustomException("source url is null");
        LotImage lotImage = new LotImage();
        Lot lot = new Lot();
        lot.setId(lotId);
        lot.setId(lotId);
        lotImage.setLot(lot);
        lotImage.setSource(source);
        return imageDAO.insert(lotImage);
    }

    @Override
    public List<LotImage> findAllByLotId(Integer itemId) throws CustomException {
        if (itemId == null)
            throw new CustomException("itemId is null");

        return imageDAO.findAllByLotId(itemId);
    }

    @Override
    public Optional<LotImage> findById(Integer id) throws CustomException {
        if (id == null)
            throw new CustomException("id is null");

        return imageDAO.findById(id);
    }

    @Override
    public void delete(Integer id) throws CustomException {
        if (id == null)
            throw new CustomException("id is null");
        imageDAO.delete(id);
    }
}
