package uz.pdp.auction.service.imp;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.dao.imp.LotDAO;
import uz.pdp.auction.model.entity.Lot;
import uz.pdp.auction.model.entity.enums.AuctionType;
import uz.pdp.auction.model.entity.enums.LotStatus;
import uz.pdp.auction.service.LotService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public class LotServiceImp implements LotService {

    private final LotDAO lotDAO = LotDAO.getInstance();
    private static LotServiceImp lotService;

    private LotServiceImp() {
    }

    public static LotServiceImp getInstance() {
        if (lotService == null)
            lotService = new LotServiceImp();
        return lotService;
    }

    @Override
    public Optional<Lot> offer(Lot lot) throws CustomException {
        if (lot.getName() == null)
            throw new CustomException("Provided Item is not valid  or null to process");
        if (lot.getOwner() == null || lot.getOwner().getId() == null)
            throw new CustomException("Provided Owner is not valid or null to process");
        if (lot.getStartPrice() <= 0)
            throw new CustomException("Provided Lot price amount is not valid");
        if(lot.getStartedAt() == null)
            throw new CustomException("Start time should be provided");
        if(lot.getFinishAt() == null)
            throw new CustomException("Finish time should be provided");
        if (lot.getStartedAt().isBefore(LocalDateTime.now()))
            throw new CustomException("Star date time should not be before now");
        if (lot.getFinishAt().isBefore(lot.getStartedAt()))
            throw new CustomException("Finish date time should not be before start date time");

        lotDAO.insert(lot);
        return lotDAO.findByNameDescriptionStartedAtFinishAt(lot);

    }

    @Override
    public Optional<Lot> findById(Integer id) throws CustomException {
        if(id == null)
            throw new CustomException("id is null");

        return lotDAO.findById(id);
    }

    @Override
    public List<Lot> findByOwnerId(Integer ownerId) throws CustomException {
        if(ownerId == null)
            throw new CustomException("owner id is null");
        return lotDAO.findByOwnerId(ownerId);
    }

    @Override
    public List<Lot> getAllByAuctionTypeAndStatus(AuctionType auctionType, LotStatus status) throws CustomException {
        if(auctionType == null)
            throw new CustomException("auction type is null");
        if(status == null)
            throw new CustomException("status is null");
        return lotDAO.getAllByAuctionTypeAndStatus(auctionType, status);
    }

    @Override
    public boolean changeStatus(Integer id, LotStatus status) throws CustomException {
        if(id == null)
            throw new CustomException("id is null");
        if(status == null)
            throw new CustomException("status is null");
        return lotDAO.changeStatus(id, status);
    }

    @Override
    public void delete(Integer id) throws CustomException {
        if(id == null)
            throw new CustomException("id is null");
        lotDAO.delete(id);
    }

    public boolean update(Lot lot) throws CustomException{
        return lotDAO.update(lot);
    }
}
