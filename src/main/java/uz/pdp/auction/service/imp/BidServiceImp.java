package uz.pdp.auction.service.imp;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.dao.imp.BidDAO;
import uz.pdp.auction.model.entity.Bid;
import uz.pdp.auction.model.entity.Lot;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.service.BidService;

import java.util.List;
import java.util.Optional;

public class BidServiceImp implements BidService {
    private static BidServiceImp imp;
    private final BidDAO bidDAO = BidDAO.getInstance();
    private BidServiceImp(){}
    public static BidServiceImp getInstance(){
        if(imp == null)
            imp = new BidServiceImp();
        return imp;
    }

    @Override
    public boolean create(Integer ownerId, Integer lotId, double amount) throws CustomException {
        if(ownerId == null)
            throw new CustomException("ownerId is null");
        if(lotId == null)
            throw new CustomException("lotId is null");
        if (amount <0 )
            throw new CustomException("amount iss not valid");
        User owner = new User();
        owner.setId(ownerId);
        Lot lot = new Lot();
        lot.setId(lotId);
        Bid bid = new Bid();
        bid.setAmount(amount);
        bid.setOwner(owner);
        bid.setLot(lot);
        return bidDAO.insert(bid);
    }

    @Override
    public List<Bid> findByLotId(Integer lotId) throws CustomException {
        if(lotId == null)
            throw new CustomException("lotId is null");
        return bidDAO.findByLotId(lotId);
    }

    @Override
    public List<Bid> findByOwnerId(Integer ownerId) throws CustomException {
        if(ownerId == null)
            throw new CustomException("ownerId is null");
        return bidDAO.findByOwnerId(ownerId);
    }

    @Override
    public Integer findCountByLotId(Integer lotId) throws CustomException {
        return bidDAO.findSizeByLotId(lotId);
    }

    @Override
    public Optional<Bid> findWinnerByLotId(Integer lotId) throws CustomException {
        return bidDAO.findWinnerByLotId(lotId);
    }
}
