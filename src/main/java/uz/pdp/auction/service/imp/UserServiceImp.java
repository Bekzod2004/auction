package uz.pdp.auction.service.imp;

import uz.pdp.auction.exception.CustomException;
import uz.pdp.auction.model.dao.imp.UserDAO;
import uz.pdp.auction.model.entity.User;
import uz.pdp.auction.model.entity.enums.UserRole;
import uz.pdp.auction.model.entity.enums.UserStatus;
import uz.pdp.auction.service.UserService;
import uz.pdp.auction.util.encoder.PasswordEncoder;

import java.util.List;
import java.util.Optional;

public class UserServiceImp implements UserService {
    private static UserServiceImp userServiceImp;
    private static final UserDAO userDao = UserDAO.getInstance();

    private UserServiceImp() {
    }

    public static UserServiceImp getInstance() {
        if (userServiceImp == null)
            userServiceImp = new UserServiceImp();
        return userServiceImp;
    }

    @Override
    public boolean signUp(String email, String password) throws CustomException {
        if (password == null || password.length() < 4)
            throw new CustomException("Password length is invalid. It should be at least 4 characters");
        User user = new User();
        user.setEmail(email);
        user.setPassword(PasswordEncoder.encode(password));
        return userDao.insert(user);
    }

    @Override
    public Optional<User> authenticate(String email, String password) throws CustomException {
        if (password == null || password.length() < 4)
            throw new CustomException("Password length is invalid. It should be at least 4 characters");
        Optional<User> user = userDao.authenticate(email, PasswordEncoder.encode(password));
        user.ifPresent(user1 -> {
            if (!user1.getStatus().equals(UserStatus.ACTIVE))
                throw new RuntimeException("User is blocked");
        });
        return user;
    }

    @Override
    public Optional<User> findById(Integer id) throws CustomException {
        if (id == null)
            throw new CustomException("Id should not be null");
        return userDao.findById(id);
    }

    @Override
    public Optional<User> findByEmail(String email) throws CustomException {
        if (email == null || email.length() < 2)
            throw new CustomException(email + " is invalid email");
        return userDao.findByEmail(email);
    }

    @Override
    public boolean changePassword(Integer id, String password) throws CustomException {
        if (id == null)
            throw new CustomException("id is null");
        if (password == null || password.length() < 4)
            throw new CustomException("Password length is invalid. It should be at least 4 characters");
        return userDao.updatePassword(id, PasswordEncoder.encode(password));
    }

    @Override
    public List<User> findAll() throws CustomException {
        return userDao.findAll();
    }

    @Override
    public boolean changePasswordByEmail(String email, String password) throws CustomException {
        if (email == null)
            throw new CustomException("email is null");
        if (password == null || password.length() < 4)
            throw new CustomException("Password length is invalid. It should be at least 4 characters");
        return userDao.changePasswordByEmail(email, PasswordEncoder.encode(password));
    }

    @Override
    public boolean changeRole(Integer id, UserRole role) throws CustomException {
        if (id == null)
            throw new CustomException("id id null");
        if (role == null)
            throw new CustomException("role is null");

        return userDao.changeRole(id, role);
    }

    @Override
    public boolean changeStatus(Integer id, UserStatus status) throws CustomException {
        if (id == null)
            throw new CustomException("id id null");
        if (status == null)
            throw new CustomException("status is null");

        return userDao.changeStatus(id, status);
    }

    @Override
    public void delete(Integer id) throws CustomException {
        userDao.delete(id);
    }
}
