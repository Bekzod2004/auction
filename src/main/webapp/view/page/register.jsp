<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
            integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N"
            crossorigin="anonymous"
    />
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css"
    />
</head>
<body>
<jsp:include page="../fragment/header.jsp"/>
<form class="position-absolute border p-3 d-flex flex-column"
      style="top: 50%; left:50%; transform: translate(-50%, -50%); row-gap: 10px;"
      action="register" method="post">
    <h2 class="text-center">Register</h2>
    <div <%=request.getAttribute("error") == null ? "hidden" : ""%> class="alert alert-danger"><i
            class="bi bi-dot"></i><%=request.getAttribute("error") == null ? "Something went wrong" : request.getAttribute("error")%>
    </div>
    <div class="input-group w-auto">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="bi bi-person-circle"></i></span>
        </div>

        <input class="form-control" type="email" placeholder="Email" name="email" required>

    </div>

    <div class="input-group w-auto">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="bi bi-key-fill"></i></span>
        </div>

        <input class="form-control" type="password" placeholder="Password" name="password" required>

    </div>

    <div class="input-group w-auto">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="bi bi-key-fill"></i></span>
        </div>
        <input class="form-control" type="password" placeholder="Pre-Password" name="pre-password" required>
    </div>
    <button class="btn btn-primary" type="submit"><i class="bi bi-pencil-square"></i> Register</button>
</form>
</body>
</html>
