<%@ page import="uz.pdp.auction.model.dto.LotDto" %>
<%@ page import="uz.pdp.auction.model.entity.User" %>
<%@ page import="uz.pdp.auction.model.entity.enums.LotStatus" %>
<%@ page import="uz.pdp.auction.model.entity.enums.UserRole" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <link
            rel="stylesheet"
            type="text/css"
            media="screen"
            href="../../static/css/main.css"
    />
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
            integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N"
            crossorigin="anonymous"
    />
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css"
    />
    <title>Lot</title>
</head>
<body>
<jsp:include page="../fragment/header.jsp"/>
<%

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy mm:hh");
    User user = (User) session.getAttribute("user");
    LotDto lot = (LotDto) request.getAttribute("lot");
    if (lot != null) {
%>
<div
        class="container-lg position-absolute border rounded"
        style="height: auto; margin: auto; left: 0; right: 0; top: 100px; background-color: white; padding-top: 10px;"
>
    <h1 style="font-family: Laila,serif"><%=lot.getName()%>
    </h1>
    <div
            class="badge badge-success position-absolute float-right"
            style="right: 10px; top: 3px; font-size: 15px"
    >
        <i class="bi bi-hammer"></i> <%=lot.getStatus()%>
    </div>
    <div
            class="badge badge-success position-absolute float-right"
            style="right: 10px; top: 30px; font-size: 15px"
    >
        <%=lot.getAuctionType()%>
    </div>
    <div class="border-bottom"></div>
    <form action="bid?lot-id=<%=lot.getId()%>" method="post" class="container">
        <div
                class="img-thumbnail"
                style="overflow: hidden; float: left; width: 45%; border: none"
        >
            <img
                    src="${pageContext.request.contextPath}/image?lot-id=<%=lot.getId()%>"
                    class="rounded float-left img-thumbnail"
                    style="margin: 10px"
                    alt=""
            />
        </div>
        <div
                class="container float-right w-50 h-50 d-flex flex-row justify-content-between flex-wrap"
                style="display: flex; gap: 10px; padding: 10px">
            <%
                if (user == null || (user.getRole().equals(UserRole.USER) && !lot.getOwner().equals(user.getId()))) {
            %>
            <div class="from-group w-auto">
                <label for="bid">Bid amount</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="bi bi-hand-index-thumb-fill"></i>
                </span>
                    </div>
                    <input type="number" name="amount" class="form-control" id="bid" required/>
                </div>
            </div>
            <%
                }
            %>
            <div class="from-group w-auto">
                <label for="price">Current Price</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="bi bi-currency-dollar"></i></span>
                    </div>
                    <input disabled type="number" name="" class="form-control" id="price"
                           placeholder="<%=lot.getWinnerBid().getAmount()%>"/>
                </div>
            </div>

            <h4 class="border-bottom w-100">Bid information</h4>
            <h5 class="w-auto">Starts at : <%=lot.getStartedAt().format(formatter)%></h5>
            <h5 class="w-auto">Finishes at : <%=lot.getFinishAt().format(formatter)%></h5>
            <div class="form-group w-50">
                <h5>All offered Bids Count - <%=lot.getBidCount()%>
                </h5>
            </div>
            <%
                if (user != null && user.getRole().equals(UserRole.USER) && !user.getId().equals(lot.getOwner())) {
            %>
            <button type="submit" class="btn w-100 btn-success">
                <i class="bi bi-cash-coin"></i>
                Make Bid
            </button>
            <%
            } else if (user != null && user.getRole().equals(UserRole.USER) && user.getId().equals(lot.getOwner())) {
            %>
            <div class="btn-group w-auto">
                <%
                    if (lot.getStatus().equals(LotStatus.NEW) || lot.getStatus().equals(LotStatus.SUSPENDED)) {
                %>
                <a type="button" class="btn btn-warning w-auto" href="update-lot?id=<%=lot.getId()%>">
                    <i class="bi bi-pencil-square"></i>
                    Edit
                </a>
                <a type="button" class="btn btn-danger w-auto" onclick="{if (window.confirm('Are you sure?')) window.location = 'delete-lot?id=<%=lot.getId()%>'}">
                    <i class="bi bi-trash"></i>
                    Remove
                </a>
                <%
                    }
                %>
            </div>
            <%
            } else if (user != null && (user.getRole().equals(UserRole.ADMIN) || user.getRole().equals(UserRole.SUPER_ADMIN))) {
            %>
            <div class="btn-group w-auto">
                <%
                    if (lot.getStatus().equals(LotStatus.NEW) || (lot.getStatus().equals(LotStatus.SUSPENDED) && lot.getFinishAt().isAfter(LocalDateTime.now()))) {
                %>
                <a type="button" class="btn btn-success w-auto" href="lot-status?id=<%=lot.getId()%>&lot-status=ACTIVE">
                    <i class="bi bi-check2-circle"></i>
                    Activate this Lot
                </a>
                <%
                    }
                    if (lot.getStatus().equals(LotStatus.ACTIVE)) {
                %>
                <a type="button" class="btn btn-warning w-auto" href="lot-status?id=<%=lot.getId()%>&lot-status=SUSPENDED">
                    <i class="bi bi-snow"></i>
                    Suspend
                </a>
                <%
                    }
                %>
                <button type="button" class="btn btn-danger w-auto" onclick="{if (window.confirm('Are you sure?')) window.location = 'delete-lot?id=<%=lot.getId()%>'}">
                    <i class="bi bi-trash"></i>
                    Remove
                </button>
            </div>
            <%
            } else {
            %>
            <a type="button" class="btn w-100 btn-success w-auto" href="login">
                <i class="bi bi-person-circle"></i>
                Login to make bid
            </a>
            <%
                }
            %>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
              <span class="input-group-text"
              ><i class="bi bi-info-square-fill"></i
              ></span>
                </div>
                <label for="desc"></label><textarea
                    name=""
                    class="form-control disabled"
                    id="desc"
                    cols="30"
                    rows="10"
                    disabled
            ><%=lot.getDescription()%></textarea>
            </div>
        </div>
    </form>
</div>

<style>
    img {
        transition: all 0.3s;
    }

    img:hover {
        cursor: pointer;
        transform: scale(1.1);
    }
</style>
<%
    }
%>
</body>
</html>
