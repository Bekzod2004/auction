<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Forgot Password</title>
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
            integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N"
            crossorigin="anonymous"
    />
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css"
    />
</head>
<body>
<jsp:include page="../fragment/header.jsp"/>
<form class="position-absolute border p-3 d-flex flex-column"
      style="top: 50%; left:50%; transform: translate(-50%, -50%); row-gap: 10px;"
      action="password-recovery" method="get">
    <h2 class="text-center">Recovery Email</h2>

    <div <%=request.getAttribute("error") == null ? "hidden" : ""%> class="alert alert-danger"><i
            class="bi bi-dot"></i><%=request.getAttribute("error") == null ? "Something went wrong" : request.getAttribute("error")%>
    </div>

    <div class="input-group w-auto">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="bi bi-person-circle"></i></span>
        </div>
        <input class="form-control" type="email" placeholder="Email" name="email" required>

    </div>

    <button class="btn btn-primary" type="submit"><i class="bi bi-chat-dots"></i> Send Code</button>

</form>

</body>
</html>
