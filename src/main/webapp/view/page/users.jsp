<%@ page import="java.util.List" %>
<%@ page import="uz.pdp.auction.model.entity.User" %>
<%@ page import="uz.pdp.auction.model.entity.enums.UserRole" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>List of Users</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
          integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
</head>

<body>

<jsp:include page="../fragment/header.jsp"/>
<div class="container w-75 align-center m-auto" style="top: 10%">

    <%
        List<User> users = (List<User>) request.getAttribute("users");
        if (users != null) {
    %>
    <div <%=request.getAttribute("error") == null ? "hidden" : ""%> class="alert alert-danger"><i
            class="bi bi-dot"></i><%=request.getAttribute("error") == null ? "Something went wrong" : request.getAttribute("error")%>
    </div>
    <table class="table">
        <caption>Total count: <%=users.size()%>
        </caption>
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Email</th>
            <th scope="col">Role</th>
            <th scope="col">Status</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>

        <tbody>
        <%
            int i = 0;
            for (User u : users) {
                i++;
        %>
        <tr>
            <td scope="row"><%=i%>
            </td>
            <td scope="row"><%=u.getEmail()%>
            </td>
            <td scope="row"><%=u.getRole()%>
            </td>
            <td scope="row"><%=u.getStatus()%>
            </td>
            <td scope="row">
                <%
                    switch (u.getStatus()) {
                        case BLOCKED:
                %>
                <a class="btn btn-warning" href="user-status?id=<%=u.getId()%>&status=ACTIVE" data-toggle="tooltip" data-placement="top" title="Activate User"><i
                        class="bi bi-unlock-fill"></i></a>
                <%
                    break;
                    case ACTIVE:
                %>
                <a class="btn btn-warning" href="user-status?id=<%=u.getId()%>&status=BLOCKED" data-toggle="tooltip" data-placement="top" title="Block User"><i
                        class="bi bi-lock-fill"></i></a>
                <%
                            break;
                        default:
                    }
                    if (((User) session.getAttribute("user")).getRole().equals(UserRole.SUPER_ADMIN)) {
                        switch (u.getRole()) {
                            case USER:
                %>
                <a class="btn btn-success" href="user-role?id=<%=u.getId()%>&role=ADMIN" data-toggle="tooltip" data-placement="top" title="Make Admin"><i
                        class="bi bi-person-fill-gear"></i></a>
                <%
                    break;
                    case ADMIN:
                %>
                <a class="btn btn-success" href="user-role?id=<%=u.getId()%>&role=USER" data-toggle="tooltip" data-placement="top" title="Make User"><i
                        class="bi bi-person-fill"></i></a>
                <%
                            break;
                            default:
                        }
                    }
                %>
                <button class="btn btn-danger" onclick="{if (window.confirm('Are you sure?')) window.location = 'delete-user?id=<%=u.getId()%>'}" data-toggle="tooltip" data-placement="top" title="Delete User" ><i class="bi bi-person-dash-fill"></i></button>
            </td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>
    <%
        }
    %>
</div>
</body>

</html>