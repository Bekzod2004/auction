<%@ page import="uz.pdp.auction.model.entity.User" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>My Account</title>
</head>
<body>
<jsp:include page="../fragment/header.jsp"/>

<h1>Email - <%=((User) session.getAttribute("user")).getEmail()%>
</h1>
<h1><a href="change-password"> Change Password</a></h1>
<h1>Role - <%=((User) session.getAttribute("user")).getRole()%>
</h1>
<h1>Status - <%=((User) session.getAttribute("user")).getStatus()%>
</h1>
</body>
</html>
