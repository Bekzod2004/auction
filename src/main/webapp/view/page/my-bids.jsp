<%@ page import="uz.pdp.auction.model.entity.User" %>
<%@ page import="java.util.List" %>
<%@ page import="uz.pdp.auction.model.dto.BidDto" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>My Bid Lots</title>
    <link
            rel="stylesheet"
            type="text/css"
            media="screen"
            href="../../static/css/header.css"
    />
    <link
            rel="stylesheet"
            type="text/css"
            media="screen"
            href="../../static/css/main.css"
    />
    <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200"
    />
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
            integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N"
            crossorigin="anonymous"
    />
    <link
            rel="stylesheet"
            type="text/css"
            media="screen"
            href="../../static/css/home.css"
    />
</head>
<body>
<jsp:include page="../fragment/header.jsp"/>
<div class="Container">

    <h1 class="border-bottom" style="border-color: blue">These ara all bids That you make</h1>

    <div class="alert alert-primary">You can monitor your activity</div>

    <div class="AuctionList">
        <%
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy mm:hh");
            User user = (User) session.getAttribute("user");

            if (request.getAttribute("bidDTOs") != null && user != null) {
                List<BidDto> bids = (List<BidDto>) request.getAttribute("bidDTOs");
                for (BidDto bid : bids) {
        %>
        <div class="AuctionListItem">
            <div class="top border-bottom"
                 style="border-bottom: rgb(210, 210, 210) 1px solid;"
                 onclick="window.location = 'lot?id=<%=bid.getLot()%>'">
                <div class="Name text-center">
                    <%=bid.getLotName()%> <br/>
                    <span class="Details"> Bid amount : <%=bid.getAmount()%> <br> Bid offered at : <%=bid.getUpdatedAt().format(formatter)%></span>
                </div>
            </div>
            <%
                if (bid.isWinner()) {
            %>
            <span class="badge badge-pill badge-success status w-auto">Winner</span>
            <%
            } else {
            %>
            <span class="badge badge-pill badge-warning status w-auto">Looser</span>
            <%
                }
                if (bid.getLotFinishTime().isAfter(LocalDateTime.now())) {
            %>
            <span class="w-auto badge badge-pill badge-success status right">Lot is active</span>
            <%
            } else {
            %>
            <span class="w-auto badge badge-pill badge-danger status right">Lot is closed</span>
            <%
                }
            %>
            <div class="bottom">
                <div class="left">Auction type : <%=bid.getAuctionType()%></div>
                <div class="right">Current Lot status : <%=bid.getLotStatus()%></div>
            </div>
        </div>
        <%
                }
            }
        %>
    </div>
</div>

</body>
</html>
