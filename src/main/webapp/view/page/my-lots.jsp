<%@ page import="uz.pdp.auction.model.dto.LotDto" %>
<%@ page import="java.util.List" %>
<%@ page import="uz.pdp.auction.model.entity.User" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>My Lots</title>

    <link
            rel="stylesheet"
            type="text/css"
            media="screen"
            href="../../static/css/header.css"
    />
    <link
            rel="stylesheet"
            type="text/css"
            media="screen"
            href="../../static/css/main.css"
    />
    <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200"
    />
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
            integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N"
            crossorigin="anonymous"
    />
    <link
            rel="stylesheet"
            type="text/css"
            media="screen"
            href="../../static/css/home.css"
    />
</head>
<body>
<jsp:include page="../fragment/header.jsp"/>
<div class="Container">

    <h1 class="border-bottom" style="border-color: blue">These ara all lots That you offer</h1>

    <div class="alert alert-primary">You can edit only Lots with New or Suspended status</div>

    <div class="AuctionList">
        <%
            User user = (User) session.getAttribute("user");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy mm:hh");
            if (request.getAttribute("lotDTOs") != null && user != null) {
                List<LotDto> lots = (List<LotDto>) request.getAttribute("lotDTOs");
                for (LotDto lot : lots) {
        %>
        <div class="AuctionListItem">
            <div class="top border-bottom"
                 style="border-bottom: rgb(210, 210, 210) 1px solid;"
                 onclick="window.location = 'lot?id=<%=lot.getId()%>'">
                <div class="Name text-center">
                    <%=lot.getName()%> <br/>
                    <span class="Details">Started at : <%=lot.getStartedAt().format(formatter)%> <br> Finishes at : <%=lot.getFinishAt().format(formatter)%></span>
                </div>
            </div>
            <span class="badge badge-pill badge-success status w-auto"><%=lot.getStatus()%></span>
            <span class="w-auto badge badge-pill badge-success status right"><%=lot.getAuctionType()%></span>
            <div class="bottom">
                <div class="left"><%=lot.getBidCount()%><br>Offered Bids</div>
                <div class="right">Current Price:
                    <br><%=lot.getWinnerBid() != null ? lot.getWinnerBid().getAmount() : lot.getStartPrice()%>
                </div>
            </div>
        </div>
        <%
                }
            }
        %>
    </div>
</div>
</body>
</html>
