<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Lot Offer</title>
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
            integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N"
            crossorigin="anonymous"
    />
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css"
    />
</head>
<body>

<jsp:include page="../fragment/header.jsp"/>
<form class="position-absolute border p-3 d-flex flex-column w-75 h-auto"
      style="top: 60px; left:50%; transform: translate(-50%); row-gap: 10px;"
      action="offer" method="post"
      enctype="multipart/form-data">

    <h2 class="text-center">Fill the New Lot fields</h2>

    <div <%=request.getAttribute("error") == null ? "hidden" : ""%> class="alert alert-danger"><i
            class="bi bi-dot"></i><%=request.getAttribute("error") == null ? "Something went wrong" : request.getAttribute("error")%>
    </div>

    <div class="input-group w-auto">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="bi bi-pencil-fill"></i></span>
        </div>
        <input class="form-control" type="text" placeholder="Name" name="name" required>
    </div>

    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"
              ><i class="bi bi-info-square-fill"></i
              ></span>
            </div>
            <label for="desc"></label><textarea
                required
                placeholder="Description"
                name="description"
                class="form-control disabled"
                id="desc"
                cols="30"
                rows="10"
        ></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="type">Please choose Auction Type : </label>
        <div class="input-group w-auto">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="bi bi-pencil-fill"></i></span>
            </div>

            <select class="form-control" id="type" required name="auction_type">
                <option>DIRECT</option>
                <option>REVERSE</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="img">Please choose an Image </label>
        <div class="input-group w-auto">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="bi bi-card-image"></i></span>
            </div>
            <input id="img" class="form-control" type="file" name="image">
        </div>
    </div>

    <div class="input-group w-auto">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="bi bi-currency-dollar"></i></span>
        </div>
        <input class="form-control" type="number" placeholder="Start price" name="start_price" required>
    </div>

    <div class="form-group">
        <label for="startAt">Starts At</label>
        <div class="input-group w-auto">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="bi bi-calendar-range"></i></span>
            </div>
            <input class="form-control" id="startAt" type="datetime-local" placeholder="Starts At" name="started_at"
                   required>
        </div>

    </div>

    <div class="form-group">
        <label for="finishAt">Finishes At</label>
        <div class="input-group w-auto">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="bi bi-calendar-range"></i></span>
            </div>
            <input class="form-control" id="finishAt" type="datetime-local" placeholder="Finish At" name="finish_at"
                   required>
        </div>
    </div>


    <button class="btn btn-primary" type="submit"><i class="bi bi-cloud-arrow-up"></i> Submit Lot</button>
</form>

</body>
</html>
