<%@ page import="uz.pdp.auction.model.entity.User" %>
<%@ page import="uz.pdp.auction.model.entity.enums.UserRole" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link
            rel="stylesheet"
            type="text/css"
            media="screen"
            href="../../static/css/header.css"
    />
    <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200"
    />
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
            integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N"
            crossorigin="anonymous"
    />
    <style>
        @import url("https://fonts.googleapis.com/css2?family=Laila&display=swap");
        * {
            font-family: laila, serif;
        }
    </style>
    <style>
        .material-symbols-outlined {
            font-variation-settings: "FILL" 1, "wght" 400, "GRAD" 0, "opsz" 48;
            position: relative;
            top: 5px;
            bottom: 5px;
        }
    </style>
</head>
<body>
<nav class="Navbar">
    <div class="brand">
        <img src="../../static/img/gavel.png" alt="Home"/>
        <div class="caption">English Auction</div>
    </div>
    <div class="menu">
        <%
            if (session.getAttribute("user") == null) {
        %>
        <a href="home" class="menuItems"><span class="material-symbols-outlined"> Home </span>Home</a>
        <a href="login"> Login</a>
        <a href="register">Register</a>
        <%
            } else if (((User) session.getAttribute("user")).getRole().equals(UserRole.USER)) {
        %>
        <a href="home" class="menuItems"><span class="material-symbols-outlined"> Home </span>Home</a>
        <a href="offer">Lot Offering</a>
        <a href="my-lots">My Lots</a>
        <a href="my-bids"> My Bids</a>
        <a
                class="border"
                style="border-radius: 100%; background-color: darkmagenta; width: 45px; height: 45px; text-align: center"
                href="account"><%=((User) session.getAttribute("user")).getEmail().toUpperCase().charAt(0)%>
        </a>
        <a href="logout">Logout</a>
        <%
            } else {
        %>
        <a href="home" class="menuItems"><span class="material-symbols-outlined"> Home </span>Home</a>
        <a href="users">View Users</a>
        <a
                class="border"
                style="border-radius: 100%; background-color: darkmagenta; width: 45px; height: 45px; text-align: center"
                href="account"><%=((User) session.getAttribute("user")).getEmail().toUpperCase().charAt(0)%>
        </a>
        <a href="logout">Logout</a>
        <%
            }
        %>
    </div>
</nav>
</body>
</html>

