package uz.pdp.auction.util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import uz.pdp.auction.util.encoder.PasswordEncoder;

public class PasswordEncoderTest {

    @Test
    public void testEncode() {
        String password = "root123";
        String encodedStr = PasswordEncoder.encode(password);
        assertNotEquals(password, encodedStr);
        assertEquals(encodedStr, PasswordEncoder.encode("root123"));
    }
}
